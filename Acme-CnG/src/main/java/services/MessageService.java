
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.MessageRepository;
import domain.Actor;
import domain.Message;

@Service
@Transactional
public class MessageService {

	// Managed repository
	@Autowired
	private MessageRepository	messageRepository;


	// Constructors
	public MessageService() {
		super();
	}


	// Supporting services
	@Autowired
	private ActorService	actorService;


	// Simple CRUD methods
	public Message create() {
		this.actorService.checkIfActor();
		Message message;
		message = new Message();
		Actor actor;
		Date now;

		actor = this.actorService.findByPrincipal();
		now = new Date(System.currentTimeMillis() - 1000);

		message.setSentMoment(now);
		message.setSender(actor);

		return message;
	}

	public Collection<Message> findAll() {
		Collection<Message> messages;
		messages = this.messageRepository.findAll();
		Assert.notNull(messages);
		return messages;
	}

	public Message findOne(final int messageId) {
		Assert.isTrue(messageId != 0);
		Message result;
		result = this.messageRepository.findOne(messageId);
		Assert.notNull(result);
		return result;
	}

	public Message save(final Message message) {
		this.actorService.checkIfActor();
		Message result;
		Assert.notNull(message);
		this.checkMessage(message);
		this.checkAttachments(message.getAttachments());
		result = this.messageRepository.save(message);
		return result;
	}

	public void delete(final Message message) {
		this.actorService.checkIfActor();
		Assert.notNull(message);
		this.messageRepository.delete(message);
	}

	// Other bussines methods

	public void checkMessage(final Message m) {
		Boolean result = true;
		if (m.getRecipient() == null || m.getSender() == null || m.getText() == null || m.getTitle() == null)
			result = false;
		Assert.isTrue(result);
	}

	public void checkAttachments(final Collection<String> attachments) {
		final String regexp = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
		for (final String s : attachments)
			Assert.isTrue(s.matches(regexp), "El formato de los adjuntos no es correcto");
	}

	public void checkReply(final Message m) {
		Boolean result = true;
		final Actor actor = this.actorService.findByPrincipal();
		if (!actor.getReceivedMessages().contains(m))
			result = false;
		Assert.isTrue(result);
	}

	public void checkForward(final Message m) {
		Boolean result = true;
		final Actor actor = this.actorService.findByPrincipal();
		if (!(actor.getReceivedMessages().contains(m) || actor.getSentMessages().contains(m)))
			result = false;
		Assert.isTrue(result);
	}

	public Message reconstruct(final Message m) {
		final Actor sender = this.actorService.findByPrincipal();
		final Message message1 = new Message();
		final Message message2 = new Message();

		message1.setId(m.getId());
		message1.setVersion(m.getVersion());
		message1.setTitle(m.getTitle());
		message1.setText(m.getText());
		message1.setSentMoment(m.getSentMoment());
		message1.setAttachments(m.getAttachments());
		message1.setSender(sender);
		message1.setRecipient(m.getRecipient());

		message2.setId(message1.getId() + 1);
		message2.setVersion(message1.getVersion() + 1);
		message2.setTitle(message1.getTitle());
		message2.setText(message1.getText());
		message2.setSentMoment(message1.getSentMoment());
		message2.setAttachments(message1.getAttachments());
		message2.setSender(message1.getSender());
		message2.setRecipient(message1.getRecipient());

		m.getSender().getSentMessages().add(message1);
		m.getRecipient().getReceivedMessages().add(message2);

		this.actorService.save(sender);
		this.actorService.save(m.getRecipient());

		if (sender.getId() == m.getRecipient().getId())
			throw new IllegalArgumentException("You cannot send a message to yourself");
		return message1;
	}

	public void deleteReceived(final Message message) {
		this.actorService.checkIfActor();
		Assert.notNull(message);
		final Actor recipient = this.actorService.findByPrincipal();
		Assert.isTrue(recipient.getId() == message.getRecipient().getId());
		this.messageRepository.delete(message);
	}

	public void deleteSent(final Message message) {
		this.actorService.checkIfActor();
		Assert.notNull(message);
		final Actor sender = this.actorService.findByPrincipal();
		Assert.isTrue(sender.getId() == message.getSender().getId());
		this.messageRepository.delete(message);
	}

	public Message reply(final int messageId) {
		Message result;
		final Message m = this.findOne(messageId);

		this.checkMessage(m);
		this.checkReply(m);

		result = this.create();

		result.setRecipient(m.getSender());
		return result;
	}

	public Message forward(final int messageId) {
		Message result;
		final Message m = this.findOne(messageId);

		this.checkMessage(m);
		this.checkForward(m);

		result = this.create();

		result.setTitle("FW: " + m.getTitle());
		result.setText(m.getText());
		result.setAttachments(m.getAttachments());

		return result;
	}

	// Get all existing actors
	public Collection<Actor> getRecipients() {
		Actor actor;
		Collection<Actor> actors = new ArrayList<>();

		actor = this.actorService.findByPrincipal();
		actors = this.actorService.findAll();

		actors.remove(actor);

		return actors;
	}
}
