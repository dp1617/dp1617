
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.CustomerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Announcement;
import domain.Application;
import domain.Comment;
import domain.Customer;
import domain.Message;
import forms.CustomerForm;

@Service
@Transactional
public class CustomerService {

	// Own repository

	@Autowired
	private CustomerRepository	customerRepository;


	// Constructor

	public CustomerService() {
		super();
	}

	// Simple CRUD methods

	public Customer create() {

		final UserAccount userAccount = new UserAccount();
		final Collection<Authority> authorities = new ArrayList<>();
		final Authority auth = new Authority();
		auth.setAuthority("CUSTOMER");
		authorities.add(auth);
		userAccount.setAuthorities(authorities);

		final Customer result = new Customer();

		result.setUserAccount(userAccount);

		Assert.notNull(result);

		return result;

	}
	public Customer findOne(final int customerId) {
		Assert.isTrue(customerId != 0);
		Customer result;

		result = this.customerRepository.findOne(customerId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Customer> findAll() {
		Collection<Customer> result;

		result = this.customerRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Customer customer) {
		Assert.notNull(customer);

		this.customerRepository.save(customer);
	}

	public void delete(final Customer customer) {
		Assert.notNull(customer);

		this.customerRepository.delete(customer);
	}

	// Adicional methods
	public void checkPhonePattern(final CustomerForm customer) {
		Assert.isTrue(customer.getPhone().matches("^(\\+\\d{1,3})?\\d{9}$"));
	}

	public void checkIsCorrectEmail(final CustomerForm customer) {
		Assert.isTrue(customer.getEmail().matches("^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$"));
	}

	public void checkStringsAreNotNull(final CustomerForm customer) {
		Assert.isTrue(!customer.getName().isEmpty());
	}

	public void checkIfCustomer() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.CUSTOMER))
				res = true;
		Assert.isTrue(res);
	}

	public Customer findByPrincipal() {
		Customer result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.customerRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public Customer reconstruct(final CustomerForm c) {
		final Customer customer = new Customer();

		final UserAccount account = new UserAccount();
		account.setPassword(c.getPassword());
		account.setUsername(c.getUsername());
		final Collection<Application> applications = new ArrayList<>();
		final Collection<Announcement> ownAnnouncements = new ArrayList<>();
		final Collection<Announcement> participatingAnnouncements = new ArrayList<>();
		final Collection<Message> sentMessages = new ArrayList<>();
		final Collection<Message> receivedMessages = new ArrayList<>();
		final Collection<Comment> receivedComments = new ArrayList<>();

		customer.setId(0);
		customer.setVersion(0);
		customer.setName(c.getName());
		customer.setPhone(c.getPhone());
		customer.setEmail(c.getEmail());
		customer.setUserAccount(account);
		customer.setApplications(applications);
		customer.setOwnAnnouncements(ownAnnouncements);
		customer.setParticipatingAnnouncements(participatingAnnouncements);
		customer.setSentMessages(sentMessages);
		customer.setReceivedMessages(receivedMessages);
		customer.setReceivedComments(receivedComments);

		if (!customer.getUserAccount().getPassword().equals(c.getSecondPassword()))
			throw new IllegalArgumentException("Passwords do not match");

		if (c.getCheckBox() == false)
			throw new IllegalArgumentException("You didn't accept the terms & conditions.");
		return customer;

	}

	public void saveForm(Customer customer) {

		Assert.notNull(customer);
		String password;
		final Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		final Collection<Authority> auths = new ArrayList<>();
		final Authority auth = new Authority();
		auth.setAuthority("CUSTOMER");
		auths.add(auth);

		password = customer.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		customer.getUserAccount().setPassword(password);

		customer.getUserAccount().setAuthorities(auths);
		customer = this.customerRepository.saveAndFlush(customer);

	}

	// Dashboard
	public Customer customerMoreApplicationsAccepted() {
		Customer result;
		result = this.customerRepository.customerMoreApplicationsAccepted();
		return result;
	}

	public Double avgOfferRequestPerCustomer() {
		Double result;
		result = this.customerRepository.avgOfferRequestPerCustomer();
		return result;
	}

	public Customer customerMoreApplicationsDenied() {
		Customer result;
		result = this.customerRepository.customerMoreApplicationsDenied();
		return result;
	}

	// Find customer by username
	public Customer getCustomerByUsername(final String username) {
		return this.customerRepository.findByCustomerByUsername(username);
	}
}
