
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.RequestRepository;
import domain.Announcement;
import domain.Application;
import domain.Comment;
import domain.Customer;
import domain.Place;
import domain.Request;

@Service
@Transactional
public class RequestService {

	// Own repository

	@Autowired
	private RequestRepository	requestRepository;


	// Constructor

	public RequestService() {
		super();
	}


	// Additional services
	@Autowired
	private CustomerService		customerService;
	@Autowired
	private ActorService		actorService;
	@Autowired
	private AnnouncementService	announcementService;


	// CRUD methods

	public Request create() {
		this.customerService.checkIfCustomer();

		Request result;
		Customer customer;
		Boolean banned;
		final Collection<Customer> acceptedCustomers = new ArrayList<>();
		final Collection<Application> applications = new ArrayList<>();
		final Collection<Comment> receivedComments = new ArrayList<>();

		result = new Request();
		banned = false;
		customer = this.customerService.findByPrincipal();

		result.setReceivedComments(receivedComments);
		result.setAcceptedCustomers(acceptedCustomers);
		result.setAuthorCustomer(customer);
		result.setApplications(applications);
		result.setBanned(banned);

		Assert.notNull(result);
		return result;
	}

	public Collection<Request> findAll() {
		this.actorService.checkIfActor();
		Collection<Request> results = new ArrayList<>();
		results = this.requestRepository.findAll();
		Assert.notNull(results);
		return results;
	}

	public Request findOne(final int requestId) {
		this.actorService.checkIfActor();
		Assert.isTrue(requestId != 0);

		Request result;
		result = this.requestRepository.findOne(requestId);

		Assert.notNull(result);

		return result;
	}

	public Request save(final Request request) {
		this.customerService.checkIfCustomer();
		Assert.notNull(request);

		this.checkDate(request);

		Request result;
		result = this.requestRepository.save(request);

		return result;
	}

	// Other methods ---------------------------------------------

	// Check pattern GPS
	public void checkPatternGps(final Place place) {
		Assert.isTrue(place.getGpsCoordinates().matches("^$|^([-+]?)([\\d]{1,2})(((\\.)(\\d+)(,)))(\\s*)(([-+]?)([\\d]{1,3})((\\.)(\\d+))?)$"));
	}

	// Search requests by keyword
	public Collection<Request> searchRequestsByKeyword(final String keyword) {
		this.checkIfActor();
		return this.requestRepository.searchRequestByKeyWord(keyword);
	}

	// Filter regarding when a request is banned
	public Collection<Announcement> filterRequestWithBan(final Collection<Request> requests) {
		final Collection<Announcement> toFilter = new ArrayList<>();

		toFilter.addAll(requests);

		return this.announcementService.filterAnnouncementsWithBan(toFilter);
	}

	// From repository: getRequestsByCustomer
	public Collection<Request> getRequestsByCustomer(final Customer c) {
		return this.requestRepository.getRequestsByCustomer(c);
	}

	// Check if customer
	public void checkIfActor() {
		this.actorService.checkIfActor();
	}

	// Check if customer
	public void checkIfCustomer() {
		this.customerService.checkIfCustomer();
	}

	// Check for plannedMoment
	public void checkDate(final Request r) {
		final Date now = new Date();
		Assert.isTrue(r.getPlannedMoment().after(now), "Date must be future");
	}

}
