
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.AnnoucementRepository;
import domain.Actor;
import domain.Announcement;
import domain.Customer;

@Service
@Transactional
public class AnnouncementService {

	/*
	 * It will be use principally for administrators to set banned announcement and for general listing
	 */

	// Own repository

	@Autowired
	private AnnoucementRepository	announcementRepository;


	// Constructor

	public AnnouncementService() {
		super();
	}


	// Additional services
	@Autowired
	private AdministratorService	administratorService;
	@Autowired
	private CustomerService			customerService;
	@Autowired
	private ActorService			actorService;


	// Necessary CRUD methods

	public Collection<Announcement> findAll() {
		Collection<Announcement> results = new ArrayList<>();
		results = this.announcementRepository.findAll();
		Assert.notNull(results);
		return results;
	}

	public Announcement findOne(final int announcementId) {
		Assert.isTrue(announcementId != 0);

		Announcement result;
		result = this.announcementRepository.findOne(announcementId);

		Assert.notNull(result);

		return result;
	}

	public Announcement saveForAdministrator(final Announcement announcement) {
		this.administratorService.checkIfAdministrator();
		Assert.notNull(announcement);

		Announcement result;
		result = this.announcementRepository.save(announcement);

		return result;
	}
	
	public Announcement saveForCustomer(final Announcement announcement) {
		this.customerService.checkIfCustomer();
		Assert.notNull(announcement);

		Announcement result;
		result = this.announcementRepository.save(announcement);

		return result;
	}

	public void delete(final Announcement announcement) {
		this.customerService.checkIfCustomer();
		Assert.notNull(announcement);

		this.announcementRepository.delete(announcement);
	}

	// Other methods ---------------------------------------

	// Filter banned only for actual customer and administrator
	public Collection<Announcement> filterAnnouncementsWithBan(final Collection<Announcement> announcements) {
		Assert.isTrue(this.actorService.authenticated() == true);

		final Actor actor = this.actorService.findByPrincipal();
		final List<Announcement> aux = new ArrayList<>();

		aux.addAll(announcements);

		if (actor instanceof Customer)
			for (int i = 0; i < aux.size(); i++)
				if (aux.get(i).getAuthorCustomer().getId() != actor.getId() && aux.get(i).getBanned() == true)
					announcements.remove(aux.get(i));

		return announcements;
	}

	// Ban a announcement (request or offer). Only available for administrators
	public void ban(final Announcement announcement) {
		this.administratorService.checkIfAdministrator();

		Assert.isTrue(announcement.getBanned() == false);

		announcement.setBanned(true);

		this.saveForAdministrator(announcement);
	}

	// Querys ----------------------------------------------

	// Ratio of offers versus requests
	public Double ratioOffersVersusRequests() {
		Double result;
		result = this.announcementRepository.ratioOffersVersusRequests();
		return result;
	}

}
