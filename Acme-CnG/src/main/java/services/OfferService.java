package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.OfferRepository;
import domain.Announcement;
import domain.Application;
import domain.Comment;
import domain.Customer;
import domain.Place;
import domain.Offer;

@Service
@Transactional
public class OfferService {

	// Own repository

	@Autowired
	private OfferRepository	offerRepository;


	// Constructor

	public OfferService() {
		super();
	}


	// Additional services
	@Autowired
	private CustomerService		customerService;
	@Autowired
	private ActorService		actorService;
	@Autowired
	private AnnouncementService	announcementService;


	// CRUD methods

	public Offer create() {
		this.customerService.checkIfCustomer();

		Offer result;
		Customer customer;
		Boolean banned;
		final Collection<Customer> acceptedCustomers = new ArrayList<>();
		final Collection<Application> applications = new ArrayList<>();
		final Collection<Comment> receivedComments = new ArrayList<>();

		result = new Offer();
		banned = false;
		customer = this.customerService.findByPrincipal();

		result.setReceivedComments(receivedComments);
		result.setAcceptedCustomers(acceptedCustomers);
		result.setAuthorCustomer(customer);
		result.setApplications(applications);
		result.setBanned(banned);

		Assert.notNull(result);
		return result;
	}

	public Collection<Offer> findAll() {
		this.actorService.checkIfActor();
		Collection<Offer> results = new ArrayList<>();
		results = this.offerRepository.findAll();
		Assert.notNull(results);
		return results;
	}

	public Offer findOne(final int OfferId) {
		this.actorService.checkIfActor();
		Assert.isTrue(OfferId != 0);

		Offer result;
		result = this.offerRepository.findOne(OfferId);

		Assert.notNull(result);

		return result;
	}

	public Offer save(final Offer Offer) {
		this.customerService.checkIfCustomer();
		Assert.notNull(Offer);

		this.checkDate(Offer);

		Offer result;
		result = this.offerRepository.save(Offer);

		return result;
	}

	// Other methods ---------------------------------------------

	// Check pattern GPS
	public void checkPatternGps(final Place place) {
		Assert.isTrue(place.getGpsCoordinates().matches("^$|^([-+]?)([\\d]{1,2})(((\\.)(\\d+)(,)))(\\s*)(([-+]?)([\\d]{1,3})((\\.)(\\d+))?)$"));
	}

	// Search Offers by keyword
	public Collection<Offer> searchOffersByKeyword(final String keyword) {
		this.checkIfActor();
		return this.offerRepository.searchOfferByKeyWord(keyword);
	}

	// Filter regarding when a Offer is banned
	public Collection<Announcement> filterOfferWithBan(final Collection<Offer> Offers) {
		final Collection<Announcement> toFilter = new ArrayList<>();

		toFilter.addAll(Offers);

		return this.announcementService.filterAnnouncementsWithBan(toFilter);
	}

	// From repository: getOffersByCustomer
	public Collection<Offer> getOffersByCustomer(final Customer c) {
		return this.offerRepository.getOffersByCustomer(c);
	}

	// Check if customer
	public void checkIfActor() {
		this.actorService.checkIfActor();
	}

	// Check if customer
	public void checkIfCustomer() {
		this.customerService.checkIfCustomer();
	}

	// Check for plannedMoment
	public void checkDate(final Offer r) {
		final Date now = new Date();
		Assert.isTrue(r.getPlannedMoment().after(now), "Date must be future");
	}

}
