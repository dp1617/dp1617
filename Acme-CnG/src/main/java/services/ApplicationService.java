
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.ApplicationRepository;
import domain.Announcement;
import domain.Application;
import domain.Customer;

@Service
@Transactional
public class ApplicationService {

	// Managed repository
	@Autowired
	private ApplicationRepository	applicationRepository;


	// Constructors
	public ApplicationService() {
		super();
	}


	// Supporting services
	@Autowired
	private ActorService		actorService;
	@Autowired
	private CustomerService		customerService;
	@Autowired
	private AnnouncementService	announcementService;


	// Simple CRUD methods
	public Application create() {
		this.actorService.checkIfActor();

		Application application;
		application = new Application();

		final Customer customer = new Customer();

		application.setCustomer(customer);

		return application;
	}

	public Collection<Application> findAll() {
		Collection<Application> applications;
		applications = this.applicationRepository.findAll();
		Assert.notNull(applications);
		return applications;
	}

	public Application findOne(final int applicationId) {
		Assert.isTrue(applicationId != 0);
		Application result;
		result = this.applicationRepository.findOne(applicationId);
		Assert.notNull(result);
		return result;
	}

	public Application save(final Application application) {
		this.actorService.checkIfActor();
		Application result;
		Assert.notNull(application);
		result = this.applicationRepository.save(application);
		return result;
	}

	public void delete(final Application application) {
		this.actorService.checkIfActor();
		Assert.notNull(application);
		this.applicationRepository.delete(application);
	}

	// Send an application to 
	public void sendApplication(final Announcement announcement) {
		Assert.isTrue(checkIfIsHerAnnouncement(announcement));
		final Application res = new Application();
		this.checkOnlyOneAnnouncement(announcement);
		this.checkPlannedMomment(announcement);
		res.setCustomer(this.customerService.findByPrincipal());
		res.setStatus("PENDING");
		res.setAnnouncement(announcement);
		this.applicationRepository.save(res);
	}

	public void checkOnlyOneAnnouncement(final Announcement announcement) {
		Boolean res = true;
		final Customer customer = this.customerService.findByPrincipal();
		for (final Application a : customer.getApplications())
			if (a.getAnnouncement().getId() == announcement.getId())
				res = false;
		Assert.isTrue(res, "Solo puedes enviar una solicitud por anuncio");
	}

	public void checkPlannedMomment(final Announcement announcement) {
		Boolean res = true;
		final Date fecha = new Date();
		if (announcement.getPlannedMoment().before(fecha))
			res = false;
		Assert.isTrue(res, "Este anuncio ya no esta activo");
	}
	
	public Boolean checkIfIsHerApplication(Application application){
		Boolean res = true;
		Customer c = customerService.findByPrincipal();
		if(application.getCustomer().getId()!=c.getId()){
			res = false;
		}
		return res;
	}
	
	public Boolean checkIfIsHerAnnouncement(Announcement announcement){
		Boolean res = true;
		Customer c = customerService.findByPrincipal();
		if(announcement.getAuthorCustomer().getId()==c.getId()){
			res = false;
		}
		return res;
	}
	
	public Boolean checkIsPending(Application application){
		Boolean res = true;
		if(!(application.getStatus().equals("PENDING"))){
			res = false;
		}
		return res;
	}

	public void acceptRequest(final Application application) {
		Assert.isTrue(checkIsPending(application));
		application.setStatus("ACCEPTED");
		this.applicationRepository.save(application);

		final Announcement a = application.getAnnouncement();
		final Collection<Customer> c = a.getAcceptedCustomers();
		c.add(application.getCustomer());
		a.setAcceptedCustomers(c);
		this.announcementService.saveForCustomer(a);

		final Customer customer = application.getCustomer();
		final Collection<Announcement> e = customer.getParticipatingAnnouncements();
		e.add(application.getAnnouncement());
		customer.setParticipatingAnnouncements(e);
		this.customerService.save(customer);
	}

	public void cancelRequest(final Application application) {
		Assert.isTrue(checkIsPending(application));
		application.setStatus("DENIED");
		this.applicationRepository.save(application);
	}

	// Other methods ---------------------------------------------

	// Get applications from announcements from a customer
	public Collection<Application> getApplicationsSentToAnnouncements(final Customer c) {
		final Collection<Application> result = new ArrayList<>();

		for (final Announcement a : c.getOwnAnnouncements())
			for (final Application ap : a.getApplications())
				if (ap.getStatus().equals("PENDING"))
					result.add(ap);

		return result;
	}

	// Average number of applications per offer or request
	public Double avgNumberOfApplicationsPerAnnouncements() {
		Double result;
		result = this.applicationRepository.avgNumberOfApplicationsPerAnnouncements();
		return result;
	}

}
