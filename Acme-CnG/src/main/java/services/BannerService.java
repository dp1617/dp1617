
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.BannerRepository;
import domain.Banner;

@Service
@Transactional
public class BannerService {

	// Managed repository
	@Autowired
	private BannerRepository		bannerRepository;

	// Supporting services
	@Autowired
	private AdministratorService	administratorService;


	// Constructors
	public BannerService() {
		super();
	}

	// Simple CRUD methods
	public Banner create() {
		this.administratorService.checkIfAdministrator();
		final Banner banner = new Banner();
		Assert.notNull(banner);
		return banner;
	}

	public Collection<Banner> findAll() {
		Collection<Banner> banners;
		banners = this.bannerRepository.findAll();
		Assert.notNull(banners);
		return banners;
	}

	public Banner findOne(final int bannerId) {
		Assert.isTrue(bannerId != 0);
		Banner banner;
		banner = this.bannerRepository.findOne(bannerId);
		Assert.notNull(banner);
		return banner;
	}

	public Banner save(final Banner banner) {
		this.administratorService.checkIfAdministrator();
		Assert.notNull(banner);
		this.bannerRepository.save(banner);
		return banner;
	}

	public void delete(final Banner banner) {
		this.administratorService.checkIfAdministrator();
		Assert.notNull(banner);
		this.bannerRepository.delete(banner);
	}

	// Other methods --------------------------------------

	// Get the banner
	public Banner getBanner() {
		this.administratorService.checkIfAdministrator();

		Banner banner = new Banner();

		for (final Banner b : this.findAll()) {
			banner = b;
			break;
		}

		return banner;
	}
	// Show banner
	public Banner showBanner() {

		Banner banner = new Banner();

		for (final Banner b : this.findAll()) {
			banner = b;
			break;
		}

		return banner;
	}

	// Check if administrator
	public void checkIfAdministrator() {
		this.administratorService.checkIfAdministrator();
	}

	//Check picture
	public void checkURL(final String picture) {
		final String regexp = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
		Assert.isTrue(picture.matches(regexp), "El formato de la imagen no es correcto");
	}
}
