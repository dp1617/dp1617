
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.ActorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;

@Service
@Transactional
public class ActorService {

	// Managed Repository
	@Autowired
	private ActorRepository	actorRepository;


	// Constructor methods
	public ActorService() {
		super();
	}

	// Supporting services

	// Simple CRUD methods

	public Actor findOne(final int actorId) {
		Assert.isTrue(actorId != 0);
		Actor result;

		result = this.actorRepository.findOne(actorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Actor> findAll() {
		Collection<Actor> result;

		result = this.actorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Actor actor) {
		Assert.notNull(actor);

		this.actorRepository.save(actor);
	}

	// Other bussines methods
	public void checkIfActor() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.ADMIN) || a.getAuthority().equals(Authority.CUSTOMER))
				res = true;
		Assert.isTrue(res);
	}

	public Actor findByPrincipal() {
		Actor result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.actorRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public boolean authenticated() {
		Boolean res = true;
		Object principal;
		SecurityContext context;
		Authentication authentication;

		context = SecurityContextHolder.getContext();
		authentication = context.getAuthentication();
		principal = authentication.getPrincipal();

		if (!(principal instanceof UserAccount))
			res = false;

		return res;
	}

	public Actor getActorByID(final Integer a) {
		final Actor actor = null;
		this.actorRepository.getActorByID(a);

		return actor;
	}

	// Dashboard
	public Collection<Actor> actorMoreMessagesReceived() {
		Collection<Actor> result;
		result = this.actorRepository.actorMoreMessagesReceived();
		return result;
	}

	public Collection<Object[]> minAvgMaxReceivedMessagesPerActor() {
		Collection<Object[]> result;
		result = this.actorRepository.minAvgMaxReceivedMessagesPerActor();
		return result;
	}

	public Collection<Object[]> minAvgMaxSentMessagesPerActor() {
		Collection<Object[]> result;
		result = this.actorRepository.minAvgMaxSentMessagesPerActor();
		return result;
	}

	public Collection<Actor> actorMoreSentMessages() {
		Collection<Actor> result;
		result = this.actorRepository.actorMoreSentMessages();
		return result;
	}

	public Collection<Actor> actorsPostedMoreTenPorcentOfComments() {
		Collection<Actor> result;
		result = this.actorRepository.actorsPostedMoreTenPorcentOfComments();
		return result;
	}

	public Collection<Actor> actorsPostedLessTenPorcentOfComments() {
		Collection<Actor> result;
		result = this.actorRepository.actorsPostedLessTenPorcentOfComments();
		return result;
	}

	// Find actor by username
	public Actor findActorByUsername(final String username) {
		return this.actorRepository.findActorByUsername(username);
	}

}
