
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import security.UserAccount;
import domain.Actor;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer> {

	@Query("select a from Actor a where a.userAccount.username = ?1")
	Actor findActorByUsername(String username);

	@Query("select a from Actor a where a.userAccount = ?1")
	Actor findByUserAccount(UserAccount userAccount);

	@Query("select a from Actor a where a = ?1")
	Actor getActorByID(Integer a);

	// Dashboard
	@Query("select a from Actor a where a.receivedMessages.size >= (select max(a.receivedMessages.size) from Actor a)")
	Collection<Actor> actorMoreMessagesReceived();

	@Query("select min(a.sentMessages.size),avg(a.sentMessages.size),max(a.sentMessages.size) from Actor a")
	Collection<Object[]> minAvgMaxSentMessagesPerActor();

	// Level A JP
	@Query("select min(a.receivedMessages.size),avg(a.receivedMessages.size),max(a.receivedMessages.size) from Actor a")
	Collection<Object[]> minAvgMaxReceivedMessagesPerActor();

	// Querys Dani
	@Query("select a from Actor a where a.sentMessages.size >= (select max(a.sentMessages.size) from Actor a)")
	Collection<Actor> actorMoreSentMessages();

	@Query("select a from Actor a where a.writtenComments.size>=(select avg(a.writtenComments.size) from Actor a)*0.1")
	Collection<Actor> actorsPostedMoreTenPorcentOfComments();

	@Query("select a from Actor a where a.writtenComments.size<=(select avg(a.writtenComments.size) from Actor a)*0.1")
	Collection<Actor> actorsPostedLessTenPorcentOfComments();
}
