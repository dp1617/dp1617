
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import security.UserAccount;
import domain.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	@Query("select c from Customer c where c.userAccount.username = ?1")
	Customer findByCustomerByUsername(String username);

	@Query("select c from Customer c where c.userAccount = ?1")
	Customer findByUserAccount(UserAccount userAccount);

	@Query("select c from Customer c where c = ?1")
	Customer getCustomerByID(Integer a);

	// Dashboard
	@Query("select c from Customer c where (select max(a) from Application a where a.status = 'ACCEPTED' and a.customer.id = c.id) >= all(select max(a) from Application a where a.status = 'ACCEPTED' group by a.customer order by count(a) desc)")
	Customer customerMoreApplicationsAccepted();

	//Query Dani
	@Query("select c from Customer c where (select max(a) from Application a where a.status = 'DENIED' and a.customer.id = c.id) >= all(select max(a) from Application a where a.status = 'ACCEPTED' group by a.customer order by count(a) desc)")
	Customer customerMoreApplicationsDenied();

	//Avg offers and requests per customer -> Adri
	@Query("select avg(c.ownAnnouncements.size) from Customer c")
	Double avgOfferRequestPerCustomer();

}
