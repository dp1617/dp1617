
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Application;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Integer> {

	// C - Average number of applications per offer or request (so, by announcements)
	@Query("select avg(a.applications.size) from Announcement a")
	Double avgNumberOfApplicationsPerAnnouncements();

}
