
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import domain.Comment;

public interface CommentRepository extends JpaRepository<Comment, Integer> {

	// B - Average number of comments posted by administrators and customers
	@Query("select avg(a.writtenComments.size) from Actor a")
	Double avgWrittenCommentsByAdministratorsAndCustomers();

	// B - Average number of comments per actor, offer, or request
	@Query("select avg(c.receivedComments.size) from Commentable c")
	Double avgCommentPerActorRequestOrOffer();

}
