
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Customer;
import domain.Offer;

@Repository
public interface OfferRepository extends JpaRepository<Offer, Integer> {

	@Query("select r from Offer r where r.authorCustomer = ?1")
	Collection<Offer> getOffersByCustomer(Customer customer);

	@Query("select o from Offer o where o.title like %?1% or o.description like %?1% or o.originPlace.address like %?1% or o.destinationPlace.address like %?1%")
	Collection<Offer> searchOfferByKeyWord(String keyword);
}
