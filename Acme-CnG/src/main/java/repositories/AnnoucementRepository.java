
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Announcement;

@Repository
public interface AnnoucementRepository extends JpaRepository<Announcement, Integer> {

	// C - Ratio of offers versus requests
	@Query("select 1.0*(select count(o) from Offer o)/count(r) from Request r")
	Double ratioOffersVersusRequests();
}
