
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Customer extends Actor {

	// Relationships

	private Collection<Announcement>	ownAnnouncements;
	private Collection<Announcement>	participatingAnnouncements;
	private Collection<Application>		applications;


	@Valid
	@OneToMany(mappedBy = "customer")
	public Collection<Application> getApplications() {
		return this.applications;
	}

	public void setApplications(final Collection<Application> applications) {
		this.applications = applications;
	}

	@Valid
	@OneToMany(mappedBy = "authorCustomer")
	public Collection<Announcement> getOwnAnnouncements() {
		return this.ownAnnouncements;
	}
	public void setOwnAnnouncements(final Collection<Announcement> ownAnnouncements) {
		this.ownAnnouncements = ownAnnouncements;
	}

	@Valid
	@ManyToMany(mappedBy = "acceptedCustomers")
	public Collection<Announcement> getParticipatingAnnouncements() {
		return this.participatingAnnouncements;
	}
	public void setParticipatingAnnouncements(final Collection<Announcement> participatingAnnouncements) {
		this.participatingAnnouncements = participatingAnnouncements;
	}

}
