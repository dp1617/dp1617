
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public abstract class Announcement extends Commentable {

	// Own attributes

	private String	title;
	private String	description;
	private Date	plannedMoment;
	private Boolean	banned;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}
	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getDescription() {
		return this.description;
	}
	public void setDescription(final String description) {
		this.description = description;
	}

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getPlannedMoment() {
		return this.plannedMoment;
	}
	public void setPlannedMoment(final Date plannedMoment) {
		this.plannedMoment = plannedMoment;
	}

	@NotNull
	public Boolean getBanned() {
		return this.banned;
	}
	public void setBanned(final Boolean banned) {
		this.banned = banned;
	}


	// Relationships

	private Collection<Application>	applications;
	private OriginPlace				originPlace;
	private DestinationPlace		destinationPlace;
	private Customer				authorCustomer;
	private Collection<Customer>	acceptedCustomers;


	@Valid
	@OneToOne(cascade = CascadeType.ALL, optional = false)
	public OriginPlace getOriginPlace() {
		return this.originPlace;
	}
	public void setOriginPlace(final OriginPlace originPlace) {
		this.originPlace = originPlace;
	}

	@Valid
	@OneToOne(cascade = CascadeType.ALL, optional = false)
	public DestinationPlace getDestinationPlace() {
		return this.destinationPlace;
	}
	public void setDestinationPlace(final DestinationPlace destinationPlace) {
		this.destinationPlace = destinationPlace;
	}

	@Valid
	@OneToMany(mappedBy = "announcement")
	public Collection<Application> getApplications() {
		return this.applications;
	}
	public void setApplications(final Collection<Application> applications) {
		this.applications = applications;
	}

	@Valid
	@ManyToOne(optional = false)
	public Customer getAuthorCustomer() {
		return this.authorCustomer;
	}
	public void setAuthorCustomer(final Customer authorCustomer) {
		this.authorCustomer = authorCustomer;
	}

	@Valid
	@ManyToMany
	public Collection<Customer> getAcceptedCustomers() {
		return this.acceptedCustomers;
	}
	public void setAcceptedCustomers(final Collection<Customer> acceptedCustomers) {
		this.acceptedCustomers = acceptedCustomers;
	}

}
