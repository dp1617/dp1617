/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AnnouncementService;
import services.ApplicationService;
import services.CommentService;
import services.CustomerService;
import controllers.AbstractController;
import domain.Actor;
import domain.Customer;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Services -------------------------------------------

	@Autowired
	private AnnouncementService	announcementService;
	@Autowired
	private CommentService		commentService;
	@Autowired
	private CustomerService		customerService;
	@Autowired
	private ActorService		actorService;
	@Autowired
	private ApplicationService	applicationService;


	// Methods --------------------------------------------

	// Dashboard for administrator

	@RequestMapping("/dashboard")
	public ModelAndView dashboard() {
		final ModelAndView result = new ModelAndView();

		//Level C - Queries

		final Double ratioOffersVersusRequests;
		final Double avgOfferRequestPerCustomer;
		final Double avgNumberOfApplicationsPerAnnouncements;
		final Customer customerMoreApplicationsAccepted;
		final Customer customerMoreApplicationsDenied;

		// Level B - Queries
		final Double avgCommentPerActorRequestOrOffer;
		final Double avgWrittenCommentsByAdministratorsAndCustomers;
		Collection<Actor> actorsPostedMoreTenPorcentOfComments;
		final Collection<Actor> actorsPostedLessTenPorcentOfComments;

		// Level A - Queries

		Collection<Object[]> minAvgMaxSentMessagesPerActor;
		Collection<Object[]> minAvgMaxReceivedMessagesPerActor;
		Collection<Actor> actorMoreSentMessages;
		Collection<Actor> actorMoreMessagesReceived;

		// Initiate variables

		ratioOffersVersusRequests = this.announcementService.ratioOffersVersusRequests();
		avgOfferRequestPerCustomer = this.customerService.avgOfferRequestPerCustomer();
		avgNumberOfApplicationsPerAnnouncements = this.applicationService.avgNumberOfApplicationsPerAnnouncements();
		customerMoreApplicationsAccepted = this.customerService.customerMoreApplicationsAccepted();
		customerMoreApplicationsDenied = this.customerService.customerMoreApplicationsDenied();

		avgCommentPerActorRequestOrOffer = this.commentService.avgCommentPerActorRequestOrOffer();
		avgWrittenCommentsByAdministratorsAndCustomers = this.commentService.avgWrittenCommentsByAdministratorsAndCustomers();
		actorsPostedMoreTenPorcentOfComments = this.actorService.actorsPostedMoreTenPorcentOfComments();
		actorsPostedLessTenPorcentOfComments = this.actorService.actorsPostedLessTenPorcentOfComments();

		minAvgMaxReceivedMessagesPerActor = this.actorService.minAvgMaxReceivedMessagesPerActor();
		minAvgMaxSentMessagesPerActor = this.actorService.minAvgMaxSentMessagesPerActor();
		actorMoreSentMessages = this.actorService.actorMoreSentMessages();
		actorMoreMessagesReceived = this.actorService.actorMoreMessagesReceived();

		// Adding objects

		result.addObject("ratioOffersVersusRequests", ratioOffersVersusRequests);
		result.addObject("avgOfferRequestPerCustomer", avgOfferRequestPerCustomer);
		result.addObject("avgNumberOfApplicationsPerAnnouncements", avgNumberOfApplicationsPerAnnouncements);
		result.addObject("customerMoreApplicationsAccepted", customerMoreApplicationsAccepted);
		result.addObject("customerMoreApplicationsDenied", customerMoreApplicationsDenied);

		result.addObject("avgCommentPerActorRequestOrOffer", avgCommentPerActorRequestOrOffer);
		result.addObject("avgWrittenCommentsByAdministratorsAndCustomers", avgWrittenCommentsByAdministratorsAndCustomers);
		result.addObject("actorsPostedMoreTenPorcentOfComments", actorsPostedMoreTenPorcentOfComments);
		result.addObject("actorsPostedLessTenPorcentOfComments", actorsPostedLessTenPorcentOfComments);

		result.addObject("minAvgMaxReceivedMessagesPerActor", minAvgMaxReceivedMessagesPerActor);
		result.addObject("minAvgMaxSentMessagesPerActor", minAvgMaxSentMessagesPerActor);
		result.addObject("actorMoreSentMessages", actorMoreSentMessages);
		result.addObject("actorMoreMessagesReceived", actorMoreMessagesReceived);

		return result;
	}
}
