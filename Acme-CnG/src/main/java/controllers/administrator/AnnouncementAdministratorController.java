/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AnnouncementService;
import controllers.AbstractController;
import domain.Announcement;
import domain.Offer;
import domain.Request;

@Controller
@RequestMapping("/announcement/administrator")
public class AnnouncementAdministratorController extends AbstractController {

	// Services -------------------------------------------

	@Autowired
	private AnnouncementService	announcementService;


	// Methods --------------------------------------------

	// Ban for administrator
	@RequestMapping("/ban")
	public ModelAndView banRequest(@RequestParam final int announcementID) {
		Announcement announcement;
		ModelAndView result = new ModelAndView("announcement/administrator/ban");

		announcement = this.announcementService.findOne(announcementID);

		this.announcementService.ban(announcement);

		if (announcement instanceof Request)
			result = new ModelAndView("redirect:/request/list.do");
		if (announcement instanceof Offer)
			result = new ModelAndView("redirect:/offer/list.do");

		return result;
	}
}
