
package controllers.administrator;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.CommentService;
import controllers.AbstractController;
import domain.Actor;
import domain.Announcement;
import domain.Comment;

@Controller
@RequestMapping("/comment/administrator")
public class CommentAdministratorController extends AbstractController {

	// Servicios
	@Autowired
	private CommentService	commentService;


	// M�todos

	// List of all written comments to actors, for administrator
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	public ModelAndView listAll() {
		ModelAndView res;
		Collection<Comment> comments = new ArrayList<>();

		comments = this.commentService.getAllWrittenCommentsToActors();

		res = new ModelAndView("comment/administrator/listAll");
		res.addObject("createdComments", comments);
		res.addObject("requestURI", "comment/administrator/listAll.do");

		return res;
	}

	// Ban a comment, for administrator
	@RequestMapping("/ban")
	public ModelAndView banRequest(@RequestParam final int commentID) {
		Comment comment;
		ModelAndView result = new ModelAndView("comment/administrator/ban");

		comment = this.commentService.findOne(commentID);

		this.commentService.ban(comment);

		if (comment.getCommentable() instanceof Announcement)
			result = new ModelAndView("redirect:/comment/id/received.do?announcementID=" + comment.getCommentable().getId());
		if (comment.getCommentable() instanceof Actor)
			result = new ModelAndView("redirect:/comment/administrator/listAll.do");

		return result;
	}

}
