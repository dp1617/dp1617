
package controllers.customer;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.CustomerService;
import services.RequestService;
import controllers.AbstractController;
import domain.Customer;
import domain.Request;

@Controller
@RequestMapping("/request/customer")
public class RequestCustomerController extends AbstractController {

	// Services ------------------------------------------------------

	@Autowired
	private RequestService	requestService;
	@Autowired
	private CustomerService	customerService;


	// Constructor ----------------------------------------------------

	public RequestCustomerController() {
		super();
	}

	// Methods --------------------------------------------------------

	// My request list, only request of actual customer
	@RequestMapping("/myList")
	public ModelAndView myRequestList() {
		ModelAndView result;
		Customer customer;
		Collection<Request> myRequests = new ArrayList<>();

		customer = this.customerService.findByPrincipal();
		myRequests = this.requestService.getRequestsByCustomer(customer);

		result = new ModelAndView("request/customer/myList");
		result.addObject("announcements", myRequests);
		result.addObject("requestURI", "request/customer/myList.do");

		return result;
	}

	// Request create
	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		Request request;

		request = this.requestService.create();
		result = this.createCreateModelAndView(request);

		return result;
	}

	// Save for create
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Request request, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = this.createCreateModelAndView(request);
			System.out.println(binding.getAllErrors());
			if (binding.getAllErrors().toString().contains("match pattern"))
				result = this.createCreateModelAndView(request, "announcement.gps.error");
			if (binding.getAllErrors().toString().contains("blank") || binding.getAllErrors().toString().contains("null"))
				result = this.createCreateModelAndView(request, "announcement.error.blank");
			if (binding.getAllErrors().toString().contains("format"))
				result = this.createCreateModelAndView(request, "announcement.error.date");
		} else
			try {
				this.requestService.save(request);
				result = new ModelAndView("redirect:/request/customer/myList.do");
			} catch (final Throwable oops) {
				if (oops.getLocalizedMessage().contains("Date must be future"))
					result = this.createCreateModelAndView(request, "announcement.plannedMommentPast.error");
				else
					result = this.createCreateModelAndView(request, "announcement.commit.error");
			}
		return result;
	}
	// Auxiliary methods ----------------------------------------------

	// Create model and view for 'create'
	protected ModelAndView createCreateModelAndView(final Request request) {
		ModelAndView result;

		result = this.createCreateModelAndView(request, null);

		return result;
	}
	protected ModelAndView createCreateModelAndView(final Request request, final String message) {
		ModelAndView result;

		result = new ModelAndView("request/customer/create");

		result.addObject("announcement", request);
		result.addObject("message", message);
		result.addObject("requestURI", "request/customer/create.do");

		return result;
	}

}
