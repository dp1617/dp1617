
package controllers.customer;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.CustomerService;
import services.OfferService;
import controllers.AbstractController;
import domain.Customer;
import domain.Offer;

@Controller
@RequestMapping("/offer/customer")
public class OfferCustomerController extends AbstractController {

	// Services ------------------------------------------------------

	@Autowired
	private OfferService	offerService;
	@Autowired
	private CustomerService	customerService;


	// Constructor ----------------------------------------------------

	public OfferCustomerController() {
		super();
	}

	// Methods --------------------------------------------------------

	// My Offer list, only Offer of actual customer
	@RequestMapping("/myList")
	public ModelAndView myOfferList() {
		ModelAndView result;
		Customer customer;
		Collection<Offer> myOffers = new ArrayList<>();

		customer = this.customerService.findByPrincipal();
		myOffers = this.offerService.getOffersByCustomer(customer);

		result = new ModelAndView("offer/customer/myList");
		result.addObject("announcements", myOffers);
		result.addObject("requestURI", "offer/customer/myList.do");

		return result;
	}

	// Offer create
	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		Offer Offer;

		Offer = this.offerService.create();
		result = this.createCreateModelAndView(Offer);

		return result;
	}

	// Save for create
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Offer offer, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = this.createCreateModelAndView(offer);
			if (binding.getAllErrors().toString().contains("match pattern"))
				result = this.createCreateModelAndView(offer, "announcement.gps.error");
			if (binding.getAllErrors().toString().contains("blank") || binding.getAllErrors().toString().contains("null"))
				result = this.createCreateModelAndView(offer, "announcement.error.blank");
			if (binding.getAllErrors().toString().contains("format"))
				result = this.createCreateModelAndView(offer, "announcement.error.date");
		} else
			try {
				this.offerService.save(offer);
				result = new ModelAndView("redirect:/offer/customer/myList.do");
			} catch (final Throwable oops) {
				if (oops.getLocalizedMessage().contains("Date must be future"))
					result = this.createCreateModelAndView(offer, "announcement.plannedMommentPast.error");
				else
					result = this.createCreateModelAndView(offer, "announcement.commit.error");
			}
		return result;
	}
	// Auxiliary methods ----------------------------------------------

	// Create model and view for 'create'
	protected ModelAndView createCreateModelAndView(final Offer offer) {
		ModelAndView result;

		result = this.createCreateModelAndView(offer, null);

		return result;
	}
	protected ModelAndView createCreateModelAndView(final Offer offer, final String message) {
		ModelAndView result;

		result = new ModelAndView("offer/customer/create");

		result.addObject("announcement", offer);
		result.addObject("message", message);
		result.addObject("requestURI", "offer/customer/create.do");

		return result;
	}

}
