/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.customer;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AnnouncementService;
import services.CustomerService;
import controllers.AbstractController;
import domain.Announcement;
import domain.Customer;
import forms.CustomerForm;

@Controller
@RequestMapping("/customer")
public class CustomerController extends AbstractController {

	// Services -------------------------------------------

	@Autowired
	private CustomerService		customerService;
	@Autowired
	private AnnouncementService	announcementService;


	// Constructor

	public CustomerController() {
		super();
	}

	// Methods --------------------------------------------

	// Listing --------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView res;
		Collection<Customer> customers;
		customers = this.customerService.findAll();

		res = new ModelAndView("customer/list");
		res.addObject("customers", customers);
		res.addObject("requestURI", "customer/list.do");
		return res;
	}

	@RequestMapping(value = "/id/list", method = RequestMethod.GET)
	public ModelAndView idList(@RequestParam final int announcementID) {
		ModelAndView res;
		Announcement announcement;
		Collection<Customer> customers = new ArrayList<>();

		announcement = this.announcementService.findOne(announcementID);
		customers = announcement.getAcceptedCustomers();

		res = new ModelAndView("customer/list");
		res.addObject("customers", customers);
		res.addObject("requestURI", "customer/list.do");
		return res;
	}

	// Creation--------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final CustomerForm customerForm = new CustomerForm();

		res = this.createFormModelAndView(customerForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos el nuevo auditor
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final CustomerForm customerForm, final BindingResult binding) {
		ModelAndView res;
		Customer customer;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(customerForm);
			System.out.println(binding.getAllErrors());
		} else
			try {

				customer = this.customerService.reconstruct(customerForm);
				this.customerService.saveForm(customer);
				res = new ModelAndView("redirect:/security/login.do");

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(customerForm);

				if (oops.getLocalizedMessage().contains("El formato del tel�fono es incorrecto") || oops.getLocalizedMessage().contains("The format of the phone is incorrect"))
					res.addObject("phoneError", "phoneError");
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					res.addObject("duplicate", "duplicate");
				if (oops.getLocalizedMessage().contains("You didn't accept the terms & conditions.") || oops.getLocalizedMessage().contains("No aceptaste los t�rminos y condiciones."))
					res.addObject("terms", "terms");
				if (oops.getLocalizedMessage().contains("Passwords do not match") || oops.getLocalizedMessage().contains("Las contrase�as no coinciden"))
					res.addObject("dontMatch", "dontMatch");
			}
		return res;
	}

	// Ancillary methods-------------------------------------
	protected ModelAndView createFormModelAndView(final CustomerForm customerForm) {
		ModelAndView res;

		res = this.createFormModelAndView(customerForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final CustomerForm customerForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("customer/create");
		res.addObject("customerForm", customerForm);
		res.addObject("message", message);

		return res;
	}
}
