
package controllers.customer;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AnnouncementService;
import services.ApplicationService;
import services.CustomerService;
import services.OfferService;
import services.RequestService;
import controllers.AbstractController;
import domain.Announcement;
import domain.Application;
import domain.Customer;
import domain.Offer;
import domain.Request;

@Controller
@RequestMapping("/application/customer")
public class ApplicationCustomerController extends AbstractController {

	// Services ------------------------------------------------------

	@Autowired
	private ApplicationService	applicationService;
	@Autowired
	private CustomerService		customerService;
	@Autowired
	private AnnouncementService	announcementService;
	@Autowired
	private RequestService		requestService;
	@Autowired
	private OfferService		offerService;


	public ApplicationCustomerController() {
		super();
	}

	// List of SENT applications
	@RequestMapping("/list")
	public ModelAndView listSentAnnouncements() {
		ModelAndView result;

		Collection<Application> aux = new ArrayList<>();
		final Collection<Application> applications = new ArrayList<>();

		aux = this.customerService.findByPrincipal().getApplications();

		for (final Application a : aux)
			applications.add(a);

		result = new ModelAndView("application/customer/list");
		result.addObject("applications", applications);
		result.addObject("requestURI", "application/customer/list.do");

		return result;
	}

	// List of RECEIVED applications
	@RequestMapping("/r/list")
	public ModelAndView listReceivedAnnouncements() {
		ModelAndView result;
		Customer customer;
		Collection<Application> applications = new ArrayList<>();

		customer = this.customerService.findByPrincipal();

		applications = this.applicationService.getApplicationsSentToAnnouncements(customer);

		result = new ModelAndView("application/customer/r/list");
		result.addObject("applications", applications);
		result.addObject("requestURI", "application/customer/r/list.do");

		return result;
	}

	// Send request

	@RequestMapping("/sendApplication")
	public ModelAndView sendRequest(@RequestParam final int announcementID) {
		ModelAndView result = null;
		Announcement announcement;

		announcement = this.announcementService.findOne(announcementID);

		try {

			result = new ModelAndView("application/customer/sendApplication");

			this.applicationService.sendApplication(announcement);

			if (announcement instanceof Request)
				result = new ModelAndView("redirect:/request/list.do");
			if (announcement instanceof Offer)
				result = new ModelAndView("redirect:/offer/list.do");

		} catch (final Throwable oops) {
			if (oops.getLocalizedMessage().contains("Solo puedes enviar una solicitud por anuncio"))
				result = this.createListModelAndViewAnnouncement(announcement, "application.commit.error.one");
			if (oops.getLocalizedMessage().contains("Este anuncio ya no esta activo"))
				result = this.createListModelAndViewAnnouncement(announcement, "application.commit.error.active");

		}

		return result;
	}
	// Accept request

	@RequestMapping("/acceptApplication")
	public ModelAndView acceptRequest(@RequestParam final int applicationID) {
		Application application;
		ModelAndView result = new ModelAndView("application/customer/acceptApplication");

		application = this.applicationService.findOne(applicationID);

		this.applicationService.acceptRequest(application);

		result = new ModelAndView("redirect:/application/customer/r/list.do");

		return result;
	}

	// Denied request

	@RequestMapping("/cancelApplication")
	public ModelAndView deniedRequest(@RequestParam final int applicationID) {
		Application application;
		ModelAndView result = new ModelAndView("application/customer/cancelApplication");

		application = this.applicationService.findOne(applicationID);

		this.applicationService.cancelRequest(application);

		result = new ModelAndView("redirect:/application/customer/r/list.do");

		return result;
	}

	// Other methods ----------------------------------

	// Create model and view for 'list' for request
	protected ModelAndView createListModelAndViewAnnouncement(final Announcement a) {
		ModelAndView result;

		result = this.createListModelAndViewAnnouncement(a, null);

		return result;
	}
	protected ModelAndView createListModelAndViewAnnouncement(final Announcement a, final String message) {
		ModelAndView result = null;

		Collection<Announcement> filter = new ArrayList<>();

		if (a instanceof Request) {
			Collection<Request> allRequests = new ArrayList<>();
			allRequests = this.requestService.findAll();
			filter = this.requestService.filterRequestWithBan(allRequests);
			result = new ModelAndView("request/list");
		}

		if (a instanceof Offer) {
			Collection<Offer> allOffers = new ArrayList<>();
			allOffers = this.offerService.findAll();
			filter = this.offerService.filterOfferWithBan(allOffers);
			result = new ModelAndView("offer/list");
		}

		result.addObject("announcements", filter);
		result.addObject("message", message);
		result.addObject("requestURI", "request/list.do");

		return result;
	}
}
