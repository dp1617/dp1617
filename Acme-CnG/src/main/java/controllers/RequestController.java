
package controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.RequestService;
import domain.Announcement;
import domain.Request;

@Controller
@RequestMapping("/request")
public class RequestController extends AbstractController {

	// Services ------------------------------------------------------

	@Autowired
	private RequestService	requestService;


	// Constructor ----------------------------------------------------
	public RequestController() {
		super();
	}

	// Methods --------------------------------------------------------

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView search(@RequestParam final String keyword) {
		ModelAndView result;
		Collection<Request> requests = new ArrayList<>();
		Collection<Announcement> filter = new ArrayList<>();

		requests = this.requestService.searchRequestsByKeyword(keyword);

		filter = this.requestService.filterRequestWithBan(requests);

		result = new ModelAndView("request/list");
		result.addObject("announcements", filter);
		result.addObject("requestURI", "request/search.do");

		return result;
	}

	// General list.
	@RequestMapping("/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<Request> allRequests = new ArrayList<>();
		Collection<Announcement> filter = new ArrayList<>();

		allRequests = this.requestService.findAll();

		filter = this.requestService.filterRequestWithBan(allRequests);

		result = new ModelAndView("request/list");
		result.addObject("announcements", filter);
		result.addObject("requestURI", "request/list.do");

		return result;
	}

}
