
package controllers;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AnnouncementService;
import services.CommentService;
import domain.Actor;
import domain.Announcement;
import domain.Comment;
import domain.Commentable;

@Controller
@RequestMapping("/comment")
public class CommentController extends AbstractController {

	// Servicios

	@Autowired
	private CommentService		commentService;
	@Autowired
	private ActorService		actorService;
	@Autowired
	private AnnouncementService	announcementService;


	// M�todos

	// Litado de comentarios enviados
	@RequestMapping(value = "/sent", method = RequestMethod.GET)
	public ModelAndView sent() {
		ModelAndView res;
		Collection<Comment> commentsAnnouncements = new ArrayList<>();
		Collection<Comment> comments = new ArrayList<>();

		comments = this.commentService.getAllWrittenCommentsToActorByAnActor();
		commentsAnnouncements = this.commentService.getAllWrittenCommentsToAnnouncementsByAnActor();

		res = new ModelAndView("comment/sent");
		res.addObject("createdComments", comments);
		res.addObject("createdCommentsAnnouncements", commentsAnnouncements);
		res.addObject("requestURI", "comment/list.do");

		return res;
	}

	@RequestMapping(value = "/id/received", method = RequestMethod.GET)
	public ModelAndView received(@RequestParam final int announcementID) {
		ModelAndView res;
		Announcement announcement;
		Collection<Comment> comments = new ArrayList<>();
		Collection<Comment> createdComments = new ArrayList<>();

		announcement = this.announcementService.findOne(announcementID);
		comments = announcement.getReceivedComments();
		createdComments = this.commentService.filterCommentsWithBan(comments);

		res = new ModelAndView("comment/id/received");
		res.addObject("createdComments", createdComments);
		res.addObject("requestURI", "comment/announcementComments.do.do");

		return res;
	}

	@RequestMapping(value = "/received", method = RequestMethod.GET)
	public ModelAndView received() {
		ModelAndView res;
		Actor actor;
		Collection<Comment> comments = new ArrayList<>();
		Collection<Comment> createdComments = new ArrayList<>();

		actor = this.actorService.findByPrincipal();
		comments = actor.getReceivedComments();
		createdComments = this.commentService.filterCommentsWithBan(comments);

		res = new ModelAndView("comment/received");
		res.addObject("createdComments", createdComments);
		res.addObject("requestURI", "comment/listReceived.do");

		return res;
	}

	// Creaci�n de comentarios

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		Comment comment;
		comment = this.commentService.create();
		res = this.createCreateModelAndView(comment);
		return res;
	}

	// Post de comentario

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "post")
	public ModelAndView save(@Valid final Comment comment, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createCreateModelAndView(comment);
		else
			try {
				this.commentService.save(comment);
				result = new ModelAndView("redirect:/comment/sent.do");
			} catch (final Throwable oops) {
				result = this.createCreateModelAndView(comment, "comment.commit.error");
			}
		return result;
	}

	// M�todos auxiliares -----------

	// Creaci�n de vista para create
	protected ModelAndView createCreateModelAndView(final Comment comment) {
		ModelAndView res;
		res = this.createCreateModelAndView(comment, null);
		return res;
	}
	protected ModelAndView createCreateModelAndView(final Comment comment, final String message) {
		ModelAndView res;
		Collection<Commentable> commentables = new ArrayList<>();

		commentables = this.commentService.getCommentables();

		res = new ModelAndView("comment/create");

		res.addObject("commentables", commentables);
		res.addObject("comment", comment);
		res.addObject("message", message);

		return res;
	}
}
