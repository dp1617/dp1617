
package controllers;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.MessageService;
import domain.Actor;
import domain.Message;

@Controller
@RequestMapping("/message")
public class MessageController extends AbstractController {

	// Servicios

	@Autowired
	private MessageService	messageService;
	@Autowired
	private ActorService	actorService;


	// M�todos

	// Listado de mensajes recibidos
	@RequestMapping(value = "/received", method = RequestMethod.GET)
	public ModelAndView received() {
		ModelAndView res;
		Actor actor;
		Collection<Message> receivedMessages = new ArrayList<>();

		actor = this.actorService.findByPrincipal();
		receivedMessages = actor.getReceivedMessages();

		res = new ModelAndView("message/received");
		res.addObject("receivedMessages", receivedMessages);
		res.addObject("requestURI", "message/received.do");

		return res;
	}

	// Borrar mensaje recibido
	@RequestMapping(value = "/deleteReceived", method = RequestMethod.GET)
	public ModelAndView deleteReceived(@RequestParam final int messageId) {
		ModelAndView result;
		Message message;

		message = this.messageService.findOne(messageId);
		try {
			this.messageService.deleteReceived(message);
		} catch (final Throwable th) {

		}

		result = new ModelAndView("redirect:/message/received.do");

		return result;
	}

	// Listado de mensajes enviados
	@RequestMapping(value = "/sent", method = RequestMethod.GET)
	public ModelAndView sent() {
		ModelAndView res;
		Actor actor;
		Collection<Message> sentMessages = new ArrayList<>();

		actor = this.actorService.findByPrincipal();
		sentMessages = actor.getSentMessages();

		res = new ModelAndView("message/sent");
		res.addObject("sentMessages", sentMessages);
		res.addObject("requestURI", "message/sent.do");

		return res;
	}

	// Borrar mensaje enviado
	@RequestMapping(value = "/deleteSent", method = RequestMethod.GET)
	public ModelAndView deleteSent(@RequestParam final int messageId) {
		ModelAndView result;
		Message message;

		message = this.messageService.findOne(messageId);
		try {
			this.messageService.deleteSent(message);
		} catch (final Throwable th) {

		}

		result = new ModelAndView("redirect:/message/sent.do");

		return result;
	}

	// Creaci�n de mensajes

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		Message message;
		message = this.messageService.create();
		res = this.createCreateModelAndView(message);
		return res;
	}

	// Post de mensaje

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Message message, final BindingResult binding) {
		ModelAndView result;
		final Message m;

		if (binding.hasErrors()) {
			result = this.createCreateModelAndView(message);
			System.out.println(binding.getAllErrors());
		} else
			try {
				m = this.messageService.reconstruct(message);
				this.messageService.save(m);
				result = new ModelAndView("redirect:/message/sent.do");
			} catch (final Throwable oops) {
				result = this.createCreateModelAndView(message, "message.commit.error");
			}
		return result;
	}

	// Reply
	@RequestMapping(value = "/reply", method = RequestMethod.GET)
	public ModelAndView reply(@Valid final int messageId) {
		ModelAndView res;
		Message mess;

		mess = this.messageService.reply(messageId);

		res = new ModelAndView("message/reply");

		res.addObject("mess", mess);
		res.addObject("requestURI", "message/reply.do");
		return res;
	}

	// Forward
	@RequestMapping(value = "/forward", method = RequestMethod.GET)
	public ModelAndView forward(@Valid final int messageId) {
		ModelAndView res;
		Message mess;

		Collection<Actor> recipients = new ArrayList<>();
		recipients = this.messageService.getRecipients();

		mess = this.messageService.forward(messageId);

		res = new ModelAndView("message/forward");

		res.addObject("recipients", recipients);
		res.addObject("mess", mess);
		res.addObject("requestURI", "message/forward.do");
		return res;
	}

	// M�todos auxiliares -----------

	// Creaci�n de vista para create
	protected ModelAndView createCreateModelAndView(final Message message) {
		ModelAndView res;
		res = this.createCreateModelAndView(message, null);
		return res;
	}

	protected ModelAndView createCreateModelAndView(final Message mess, final String message) {
		ModelAndView res;
		Collection<Actor> recipients = new ArrayList<>();

		recipients = this.messageService.getRecipients();

		res = new ModelAndView("message/create");

		res.addObject("recipients", recipients);
		res.addObject("mess", mess);
		res.addObject("message", message);

		return res;
	}

}
