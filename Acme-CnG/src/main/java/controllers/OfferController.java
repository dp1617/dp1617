
package controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.OfferService;
import domain.Announcement;
import domain.Offer;

@Controller
@RequestMapping("/offer")
public class OfferController extends AbstractController {

	// Services ------------------------------------------------------

	@Autowired
	private OfferService	offerService;


	// Constructor ----------------------------------------------------
	public OfferController() {
		super();
	}

	// Methods --------------------------------------------------------

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView searchO(@RequestParam final String keyword) {
		ModelAndView result;
		Collection<Offer> offers = new ArrayList<>();
		Collection<Announcement> filter = new ArrayList<>();

		offers = this.offerService.searchOffersByKeyword(keyword);

		filter = this.offerService.filterOfferWithBan(offers);

		result = new ModelAndView("offer/list");
		result.addObject("announcements", filter);
		result.addObject("requestURI", "offer/search.do");

		return result;
	}

	// General list.
	@RequestMapping("/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<Offer> allOffers = new ArrayList<>();
		Collection<Announcement> filter = new ArrayList<>();

		allOffers = this.offerService.findAll();

		filter = this.offerService.filterOfferWithBan(allOffers);

		result = new ModelAndView("offer/list");
		result.addObject("announcements", filter);
		result.addObject("requestURI", "offer/list.do");

		return result;
	}

}
