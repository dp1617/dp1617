<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n -->

<jsp:useBean id="messageService" class="services.MessageService"
				scope="page" />

<form:form action="message/create.do" modelAttribute="mess">

	<!-- Atributos -->

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="sender" />
	<form:hidden path="sentMoment" />
	
	<jstl:if test="${requestURI.contains('reply.do')}">
		<form:hidden path="recipient" />
	</jstl:if>
	
	<jstl:if test="${requestURI.contains('forward.do')}">
		<form:hidden path="title" />
		<form:hidden path="text" />
		<form:hidden path="attachments" />
	</jstl:if>

	<jstl:if test="${!requestURI.contains('reply.do')}">	
		<form:label path="recipient">
       		<spring:message code="message.recipient" />
    	</form:label>    
    	<form:select id="recipient" path="recipient"> 
    		<jstl:forEach var="r" items="${recipients}">
       	   		<form:option label="${r.name}" value="${r.id}" />
       		</jstl:forEach>
    	</form:select>
    	<form:errors path="recipient" cssClass="error" />
    </jstl:if>

	<jstl:if test="${!requestURI.contains('forward.do')}">
		<acme:textbox code="message.title" path="title" />

		<acme:textbox code="message.text" path="text" />

		<acme:textbox code="message.attachments" path="attachments" />
	</jstl:if>

	<br />

	<!-- Acciones -->

	<acme:submit name="save" code="message.save" />

	<acme:cancel url="" code="message.cancel" />

</form:form>