<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jstl:if test="${requestURI.contains('received.do')}">

	<display:table name="receivedMessages" id="row"
		requestURI="${requestURI}" pagesize="5" class="displaytag">
		
		<spring:message code="message.sender" var="senderHeader" />
		<display:column property="sender.name" title="${senderHeader}" />

		<spring:message code="message.title" var="titleHeader" />
		<display:column property="title" title="${titleHeader}" sortable="true" />

		<spring:message code="message.text" var="textHeader" />
		<display:column property="text" title="${textHeader}" sortable="true" />

		<fmt:formatDate var="sentMoment" value="${row.sentMoment}"
			pattern="dd/MM/yyyy HH:mm" />
		<spring:message code="message.sentMoment" var="sentMomentHeader" />
		<display:column value="${sentMoment}" title="${sentMomentHeader}"
			sortable="true" />

		<spring:message code="message.attachments" var="attachmentsHeader" />
		<display:column property="attachments" title="${attachmentsHeader}" sortable="true" />
		
		<display:column>
   			<acme:confirmation
    		url="message/deleteReceived.do?messageId=${row.id}"
    		code="message.delete" codeConfirm="message.confirm.delete" />
  		</display:column>
  		
  		<display:column>
			<acme:button
				href="message/reply.do?messageId=${row.id}"
				name="reply" code="message.reply" />
		</display:column>
		
		<display:column>
			<acme:button
				href="message/forward.do?messageId=${row.id}"
				name="forward" code="message.forward" />
		</display:column>

	</display:table>
</jstl:if>


<jstl:if test="${requestURI.contains('sent.do')}">

	<display:table name="sentMessages" id="row"
		requestURI="${requestURI}" pagesize="5" class="displaytag">
	
		<spring:message code="message.recipient" var="recipientHeader" />
		<display:column property="recipient.name" title="${recipientHeader}" sortable="true" />

		<spring:message code="message.title" var="titleHeader" />
		<display:column property="title" title="${titleHeader}" sortable="true" />

		<spring:message code="message.text" var="textHeader" />
		<display:column property="text" title="${textHeader}" sortable="true" />

		<fmt:formatDate var="sentMoment" value="${row.sentMoment}"
			pattern="dd/MM/yyyy HH:mm" />
		<spring:message code="message.sentMoment" var="sentMomentHeader" />
		<display:column value="${sentMoment}" title="${sentMomentHeader}"
			sortable="true" />
		
		<spring:message code="message.attachments" var="attachmentsHeader" />
		<display:column property="attachments" title="${attachmentsHeader}" sortable="true" />
		
		<display:column>
   			<acme:confirmation
    		url="message/deleteSent.do?messageId=${row.id}"
    		code="message.delete" codeConfirm="message.confirm.delete" />
  		</display:column>
  		
  		<display:column>
			<acme:button
				href="message/forward.do?messageId=${row.id}"
				name="forward" code="message.forward" />
		</display:column>

	</display:table>
</jstl:if>	