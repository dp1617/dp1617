<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="applications" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<spring:message code="application.status" var="statusHeader" />
	<display:column property="status" title="${statusHeader}" sortable="true" />

	<spring:message code="application.customer" var="customerHeader" />
	<display:column property="customer.name" title="${customerHeader}" />
	
	<spring:message code="application.announcement" var="announcementHeader" />
	<display:column property="announcement.title" title="${announcementHeader}" />
	
	<jsp:useBean id="loginService" class="security.LoginService"
			scope="page" />
	<security:authorize access="hasRole('CUSTOMER')">
		<display:column title="${acceptHeader}" sortable="false">
			<jstl:if test="${row.getAnnouncement().getAuthorCustomer().getUserAccount().getId() == loginService.getPrincipal().getId()}">
				<jstl:if test="${row.status == 'PENDING'}">
					<input type="submit" name="accept"
						value="<spring:message code="application.accept" />"
						onclick="javascript: window.location.replace('application/customer/acceptApplication.do?applicationID=${row.id}');" />&nbsp;
				</jstl:if>
			</jstl:if>
		</display:column>

		<display:column title="${cancelHeader}" sortable="false">
			<jstl:if test="${row.getAnnouncement().getAuthorCustomer().getUserAccount().getId() == loginService.getPrincipal().getId()}">
				<jstl:if test="${row.status == 'PENDING'}">
					<input type="submit" name="cancel"
						value="<spring:message code="application.denied" />"
						onclick="javascript: window.location.replace('application/customer/cancelApplication.do?applicationID=${row.id}');" />&nbsp;
				</jstl:if>
			</jstl:if>
		</display:column>
	</security:authorize>
	
	
</display:table>

	

	