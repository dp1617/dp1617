<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="customer/create.do" modelAttribute="customerForm">
	
	<!-- Personal data -->
	
	<b><spring:message code="customer.create.personalData" /></b>

	<br />

	<acme:textbox code="customer.name" path="name" />

	<acme:textbox code="customer.email" path="email" />

	<acme:textbox code="customer.phone" path="phone" />
	
	<!-- Usuario y contraseņa -->
	
	<b><spring:message code="customer.create.loginData" /></b>

	<br />

	<acme:textbox code="customer.username" path="username" />

	<acme:password code="customer.password" path="password" />

	<acme:password code="customer.secondPassword"
		path="secondPassword" />

	<br />
	
	<!-- Aceptar para continuar -->
	
	<acme:checkbox href="actor/terms.do" checkboxCode="customer.create.checkBox" hrefCode="customer.create.moreInfo"/>

	<br />
	<br />
	
	<!-- Acciones -->

	<acme:submit name="save" code="customer.save" />

	<acme:cancel url="" code="customer.cancel" />

</form:form>

<br>

<!-- Errores -->

<jstl:if test="${phoneError != null}">
	<span class="message"><spring:message code="${phoneError}" /></span>
</jstl:if>

<jstl:if test="${duplicate != null}">
	<span class="message"><spring:message code="${duplicate}" /></span>
</jstl:if>

<jstl:if test="${dontMatch != null}">
	<span class="message"><spring:message code="${dontMatch}" /></span>
</jstl:if>

<jstl:if test="${terms != null}">
	<span class="message"><spring:message code="${terms}" /></span>
</jstl:if>
