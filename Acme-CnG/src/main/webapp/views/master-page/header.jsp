<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<div>
	<img src="images/logo.png" alt="Acme Car'n Go! Co., Inc." />
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize access="hasRole('ADMIN')">
			<li><a class="fNiv" href="customer/list.do"><spring:message
						code="master.page.customer.list" /></a></li>
			<li><a class="fNiv"><spring:message
						code="master.page.customer.announcements" /></a>
			<ul>
					<li class="arrow"></li>
					<li><a href="request/list.do"><spring:message
								code="master.page.customer.announcement.adminisrator.requests.list" /></a></li>
					<li><a href="offer/list.do"><spring:message
								code="master.page.customer.announcement.adminisrator.offer.list" /></a></li>
				</ul></li>
			<li><a class="fNiv" href="banner/administrator/edit.do"><spring:message
						code="master.page.admin.editbanner" /></a></li>
		</security:authorize>

		<security:authorize access="hasRole('CUSTOMER')">

			<li><a class="fNiv"><spring:message
						code="master.page.customer.announcement.requests" /></a>
			<ul>
					<li class="arrow"></li>
					<li><a href="request/list.do"><spring:message
								code="master.page.customer.announcement.requests.list" /></a></li>
					<li><a href="request/customer/myList.do"><spring:message
								code="master.page.customer.announcement.list.myrequests" /></a></li>
					<li><a href="request/customer/create.do"><spring:message
								code="master.page.customer.announcement.request.create" /></a></li>
				</ul></li>

			<li><a class="fNiv"><spring:message
						code="master.page.customer.announcement.offers" /></a>
			<ul>
					<li class="arrow"></li>
					<li><a href="offer/list.do"><spring:message
								code="master.page.customer.announcement.offers.list" /></a></li>
					<li><a href="offer/customer/myList.do"><spring:message
								code="master.page.customer.announcement.list.myoffers" /></a></li>
					<li><a href="offer/customer/create.do"><spring:message
								code="master.page.customer.announcement.offer.create" /></a></li>
				</ul></li>

			<li><a class="fNiv"><spring:message
						code="master.page.announcement" /></a>
			<ul>
					<li class="arrow"></li>
					<li><a href="application/customer/list.do"><spring:message
								code="master.page.customer.announcement.application.mySentApplications" /></a></li>
					<li><a href="application/customer/r/list.do"><spring:message
								code="master.page.customer.announcement.application.myReceivedApplications" /></a></li>
				</ul></li>

		</security:authorize>

		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message
						code="master.page.login" /></a></li>
			<li><a class="fNiv" href="customer/create.do"><spring:message
						code="master.page.customer.create" /></a></li>
		</security:authorize>

		<security:authorize access="isAuthenticated()">

			<li><a class="fNiv"><spring:message
						code="master.page.comments" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="comment/sent.do"><spring:message
								code="master.page.comments.sent" /></a></li>
					<li><a href="comment/received.do"><spring:message
								code="master.page.comments.received" /></a></li>
					<li><a href="comment/create.do"><spring:message
								code="master.page.comments.create" /></a></li>
					<security:authorize access="hasRole('ADMIN')">
						<li><a href="comment/administrator/listAll.do"><spring:message
									code="master.page.comments.listAll" /></a></li>
					</security:authorize>
				</ul></li>

			<li><a class="fNiv"><spring:message
						code="master.page.messages" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="message/sent.do"><spring:message
								code="master.page.messages.sent" /></a></li>
					<li><a href="message/received.do"><spring:message
								code="master.page.messages.received" /></a></li>
					<li><a href="message/create.do"><spring:message
								code="master.page.messages.create" /></a></li>
				</ul></li>

			<li><a class="fNiv"> <spring:message
						code="master.page.profile" /> (<security:authentication
						property="principal.username" />)
			</a>

				<ul>
					<li class="arrow"></li>

					<security:authorize access="hasRole('ADMIN')">
						<li><a href="administrator/dashboard.do"><spring:message
									code="master.page.administrator.dashboard" /></a></li>
					</security:authorize>

					<li><a href="j_spring_security_logout"><spring:message
								code="master.page.logout" /> </a></li>

				</ul></li>

		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

