<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- LEVEL C -->

<fieldset>
	<legend><b><spring:message code="administrator.ratioOffersVersusRequests"/></b></legend>
	
	<spring:message code="administrator.ratioOffersVersusRequests" /><br>
	<jstl:out value="${ratioOffersVersusRequests}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.avgOfferRequestPerCustomer"/></b></legend>
	
	<spring:message code="administrator.avgOfferRequestPerCustomer" /><br>
	<jstl:out value="${avgOfferRequestPerCustomer}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.avgNumberOfApplicationsPerAnnouncements"/></b></legend>
	
	<spring:message code="administrator.avgNumberOfApplicationsPerAnnouncements" /><br>
	<jstl:out value="${avgNumberOfApplicationsPerAnnouncements}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.customerMoreApplicationsAccepted"/></b></legend>
	
	<spring:message code="administrator.customerMoreApplicationsAccepted" /><br>
	<jstl:out value="${customerMoreApplicationsAccepted}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.customerMoreApplicationsDenied"/></b></legend>
	
	<spring:message code="administrator.customerMoreApplicationsDenied" /><br>
	<jstl:out value="${customerMoreApplicationsDenied}"></jstl:out><br>
</fieldset>


<!-- LEVEL B -->

<fieldset>
	<legend><b><spring:message code="administrator.avgCommentPerActorRequestOrOffer"/></b></legend>
	
	<spring:message code="administrator.avgCommentPerActorRequestOrOffer" /><br>
	<jstl:out value="${avgCommentPerActorRequestOrOffer}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.avgWrittenCommentsByAdministratorsAndCustomers"/></b></legend>
	
	<spring:message code="administrator.avgWrittenCommentsByAdministratorsAndCustomers" /><br>
	<jstl:out value="${avgWrittenCommentsByAdministratorsAndCustomers}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.actorsPostedMoreTenPorcentOfComments"/></b></legend>
	
	<display:table name="actorsPostedMoreTenPorcentOfComments" id="row"
	requestURI="${requestURI}" pagesize="6" class="displaytag">
	
	<jstl:out value="${actorsPostedMoreTenPorcentOfComments}"></jstl:out>

</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.actorsPostedLessTenPorcentOfComments"/></b></legend>
	
	<display:table name="actorsPostedLessTenPorcentOfComments" id="row"
	requestURI="${requestURI}" pagesize="5" class="displaytag">
	
	<jstl:out value="${actorsPostedLessTenPorcentOfComments}"></jstl:out>

</display:table>
</fieldset>

<!-- LEVEL A -->

<fieldset>
	<legend><b><spring:message code="administrator.minAvgMaxSentMessagesPerActor"/></b></legend>
	
	<display:table class="displaytag" name="minAvgMaxSentMessagesPerActor" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="min" property="[0]"/>
  		<acme:column code="avg" property="[1]"/>
  		<acme:column code="max" property="[2]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.minAvgMaxReceivedMessagesPerActor"/></b></legend>
	
	<display:table class="displaytag" name="minAvgMaxReceivedMessagesPerActor" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="min" property="[0]"/>
  		<acme:column code="avg" property="[1]"/>
  		<acme:column code="max" property="[2]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.actorMoreSentMessages"/></b></legend>
	
	<display:table name="actorMoreSentMessages" id="row"
	requestURI="${requestURI}" pagesize="5" class="displaytag">
	
	<jstl:out value="${actorMoreSentMessages}"></jstl:out>

</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.actorMoreMessagesReceived"/></b></legend>
	
	<spring:message code="administrator.actorMoreMessagesReceived" /><br>
	<jstl:out value="${actorMoreMessagesReceived}"></jstl:out><br>
</fieldset>

