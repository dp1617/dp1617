<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jsp:useBean id="commentService" class="services.CommentService"
	scope="page" />

<jstl:if test="${requestURI.contains('list.do')}">

	<div class="tituloComments">
		<spring:message code="comment.toActors" />
	</div>

	<display:table name="createdComments" id="row"
		requestURI="${requestURI}" pagesize="5" class="displaytag">

		<jstl:set var="st" value="none" />
		<jstl:if test="${row.getBanned() == true}">
			<jstl:set var="st" value="background-color:#31F600" />
		</jstl:if>

		<spring:message code="comment.title" var="titleHeader" />
		<display:column property="title" title="${titleHeader}"
			sortable="true" style="${st}" />

		<spring:message code="comment.text" var="textHeader" />
		<display:column property="text" title="${textHeader}" sortable="true"
			style="${st}" />

		<fmt:formatDate var="postedMoment" value="${row.postedMoment}"
			pattern="dd/MM/yyyy HH:mm" />
		<spring:message code="comment.postedMoment" var="postedMomentHeader" />
		<display:column value="${postedMoment}" title="${postedMomentHeader}"
			sortable="true" style="${st}" />

		<spring:message code="comment.actor" var="nameHeader" />
		<display:column property="commentable.name" title="${nameHeader}"
			sortable="true" style="${st}" />

	</display:table>
	<br />
	<br />
	<div class="tituloComments">
		<spring:message code="comment.toAnnouncement" />
	</div>

	<display:table name="createdCommentsAnnouncements" id="row"
		requestURI="${requestURI}" pagesize="5" class="displaytag">

		<jstl:set var="st" value="none" />
		<jstl:if test="${row.getBanned() == true}">
			<jstl:set var="st" value="background-color:#31F600" />
		</jstl:if>

		<spring:message code="comment.title" var="titleHeader" />
		<display:column property="title" title="${titleHeader}"
			sortable="true" style="${st}" />

		<spring:message code="comment.text" var="textHeader" />
		<display:column property="text" title="${textHeader}" sortable="true"
			style="${st}" />

		<fmt:formatDate var="postedMoment" value="${row.postedMoment}"
			pattern="dd/MM/yyyy HH:mm" />
		<spring:message code="comment.postedMoment" var="postedMomentHeader" />
		<display:column value="${postedMoment}" title="${postedMomentHeader}"
			sortable="true" style="${st}" />

		<spring:message code="comment.announcement" var="titleHeader" />
		<display:column property="commentable.title" title="${titleHeader}"
			sortable="true" style="${st}" />

	</display:table>

</jstl:if>

<jstl:if test="${requestURI.contains('listReceived.do')}">

	<display:table name="createdComments" id="row"
		requestURI="${requestURI}" pagesize="5" class="displaytag">

		<jstl:set var="st" value="none" />
		<jstl:if test="${row.getBanned() == true}">
			<jstl:set var="st" value="background-color:#31F600" />
		</jstl:if>

		<spring:message code="comment.title" var="titleHeader" />
		<display:column property="title" title="${titleHeader}"
			sortable="true" style="${st}" />

		<spring:message code="comment.text" var="textHeader" />
		<display:column property="text" title="${textHeader}" sortable="true"
			style="${st}" />

		<fmt:formatDate var="postedMoment" value="${row.postedMoment}"
			pattern="dd/MM/yyyy HH:mm" />
		<spring:message code="comment.postedMoment" var="postedMomentHeader" />
		<display:column value="${postedMoment}" title="${postedMomentHeader}"
			sortable="true" style="${st}" />

		<spring:message code="comment.actor" var="nameHeader" />
		<display:column property="commentable.name" title="${nameHeader}"
			sortable="true" style="${st}" />

	</display:table>

</jstl:if>

<jstl:if test="${requestURI.contains('announcementComments.do')}">

	<display:table name="createdComments" id="row"
		requestURI="${requestURI}" pagesize="5" class="displaytag">

		<jstl:set var="st" value="none" />
		<jstl:if test="${row.getBanned() == true}">
			<jstl:set var="st" value="background-color:#31F600" />
		</jstl:if>

		<spring:message code="comment.title" var="titleHeader" />
		<display:column property="title" title="${titleHeader}"
			sortable="true" style="${st}" />

		<spring:message code="comment.text" var="textHeader" />
		<display:column property="text" title="${textHeader}" sortable="true"
			style="${st}" />

		<fmt:formatDate var="postedMoment" value="${row.postedMoment}"
			pattern="dd/MM/yyyy HH:mm" />
		<spring:message code="comment.postedMoment" var="postedMomentHeader" />
		<display:column value="${postedMoment}" title="${postedMomentHeader}"
			sortable="true" style="${st}" />

		<spring:message code="comment.announcement" var="titleHeader" />
		<display:column property="commentable.title" title="${titleHeader}"
			sortable="true" style="${st}" />

		<security:authorize access="hasRole('ADMIN')">
			<display:column title="" style="${st}">
				<jstl:if test="${row.banned == false}">
					<input type="submit"
						value="<spring:message code="comment.administrator.ban" />"
						onclick="javascript: window.location.replace('comment/administrator/ban.do?commentID=${row.id}')" />
				</jstl:if>
			</display:column>
		</security:authorize>

	</display:table>

</jstl:if>


<jstl:if test="${requestURI.contains('listAll.do')}">

	<display:table name="createdComments" id="row"
		requestURI="${requestURI}" pagesize="5" class="displaytag">

		<jstl:set var="st" value="none" />
		<jstl:if test="${row.getBanned() == true}">
			<jstl:set var="st" value="background-color:#31F600" />
		</jstl:if>

		<spring:message code="comment.title" var="titleHeader" />
		<display:column property="title" title="${titleHeader}"
			sortable="true" style="${st}" />

		<spring:message code="comment.text" var="textHeader" />
		<display:column property="text" title="${textHeader}" sortable="true"
			style="${st}" />

		<fmt:formatDate var="postedMoment" value="${row.postedMoment}"
			pattern="dd/MM/yyyy HH:mm" />
		<spring:message code="comment.postedMoment" var="postedMomentHeader" />
		<display:column value="${postedMoment}" title="${postedMomentHeader}"
			sortable="true" style="${st}" />

		<spring:message code="comment.actor" var="nameHeader" />
		<display:column property="commentable.name" title="${nameHeader}"
			sortable="true" style="${st}" />

		<security:authorize access="hasRole('ADMIN')">
			<display:column title="" style="${st}">
				<jstl:if test="${row.banned == false}">
					<input type="submit"
						value="<spring:message code="comment.administrator.ban" />"
						onclick="javascript: window.location.replace('comment/administrator/ban.do?commentID=${row.id}')" />
				</jstl:if>
			</display:column>
		</security:authorize>

	</display:table>

</jstl:if>

<jstl:forEach var="c" items="${createdComments}">
	<jstl:if test="${c.banned == true}">
		<jstl:set var="aux" value="${1}" />
	</jstl:if>
</jstl:forEach>

<jstl:forEach var="c1" items="${createdCommentsAnnouncements}">
	<jstl:if test="${c1.banned == true}">
		<jstl:set var="aux" value="${2}" />
	</jstl:if>
</jstl:forEach>

<jstl:if test="${aux == 1 || aux == 2}">
	<p style="color: #31F600; font-size: 20px;">
		<spring:message code="comment.banned.green" />
	</p>
</jstl:if>
