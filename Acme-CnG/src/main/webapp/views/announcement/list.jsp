<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jstl:if test="${requestURI.contains('request')}">
	<form method="POST" action="request/search.do" id="search"
		name="search">
		<input type="text" id="keyword" name="keyword" /> <input
			type="submit" value="<spring:message code="announcement.search"/>" />
	</form>
</jstl:if>

<jstl:if test="${requestURI.contains('offer')}">
	<form method="POST" action="offer/search.do" id="search" name="search">
		<input type="text" id="keyword" name="keyword" /> <input
			type="submit" value="<spring:message code="announcement.search"/>" />
	</form>
</jstl:if>

<display:table name="announcements" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<jstl:set var="st" value="none" />
	<jstl:if test="${row.getBanned() == true}">
		<jstl:set var="st" value="background-color:#31F600" />
	</jstl:if>

	<spring:message code="announcement.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true"
		style="${st}" />

	<spring:message code="announcement.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}"
		style="${st}" />

	<spring:message code="announcement.originPlace.address"
		var="oAddressHeader" />
	<display:column property="originPlace.address"
		title="${oAddressHeader}" sortable="true" style="${st}" />

	<spring:message code="announcement.originPlace.gpsCoordinates"
		var="oGpsCoordinatesHeader" />
	<display:column property="originPlace.gpsCoordinates"
		title="${oGpsCoordinatesHeader}" sortable="true" style="${st}" />

	<spring:message code="announcement.destinationPlace.address"
		var="dAddressHeader" />
	<display:column property="destinationPlace.address"
		title="${dAddressHeader}" sortable="true" style="${st}" />

	<spring:message code="announcement.destinationPlace.gpsCoordinates"
		var="dGpsCoordinatesHeader" />
	<display:column property="destinationPlace.gpsCoordinates"
		title="${dGpsCoordinatesHeader}" sortable="true" style="${st}" />

	<fmt:formatDate var="plannedMoment" value="${row.plannedMoment}"
		pattern="dd/MM/yyyy HH:mm" />
	<spring:message code="announcement.plannedMoment"
		var="plannedMomentHeader" />
	<display:column property="plannedMoment" title="${plannedMomentHeader}"
		sortable="true" style="${st}" sortProperty="plannedMoment"
	format="{0,date,dd/MM/YYYY HH:mm}"/>
	 />

	<!-- If it's customer, only the actual customer have access to his or her announcement's customers -->

	<security:authorize access="hasRole('CUSTOMER')">
		<jsp:useBean id="loginService" class="security.LoginService"
			scope="page" />
		<spring:message code="announcement.acceptedCustomers"
			var="acceptedCustomers" />
		<display:column title="${acceptedCustomers}" style="${st}">
			<jstl:if
				test="${row.getAuthorCustomer().getUserAccount().getId() == loginService.getPrincipal().getId()}">
				<acme:button href="customer/id/list.do?announcementID=${row.id}"
					name="see" code="see" />
			</jstl:if>
		</display:column>
		<display:column style="${st}">
			<jstl:if
				test="${row.getAuthorCustomer().getUserAccount().getId() != loginService.getPrincipal().getId()}">
				<acme:button
					href="application/customer/sendApplication.do?announcementID=${row.id}"
					name="sendRequest" code="sendRequest" />
			</jstl:if>
		</display:column>
	</security:authorize>

	<!-- If it's an administrator, no restriction is applied -->

	<security:authorize access="hasRole('ADMIN')">
		<spring:message code="announcement.acceptedCustomers"
			var="acceptedCustomers" />
		<display:column title="${acceptedCustomers}" style="${st}">
			<acme:button href="customer/id/list.do?announcementID=${row.id}"
				name="see" code="see" />
		</display:column>
	</security:authorize>

	<!-- TODO -->
	<spring:message code="announcement.receivedComments"
		var="receivedComments" />
	<display:column title="${receivedComments}" style="${st}">
		<acme:button href="comment/id/received.do?announcementID=${row.id}"
			name="see" code="see" />
	</display:column>

	<security:authorize access="hasRole('ADMIN')">
		<display:column title="" style="${st}">
			<jstl:if test="${row.banned == false}">
				<input type="submit"
					value="<spring:message code="announcement.administrator.ban" />"
					onclick="javascript: window.location.replace('announcement/administrator/ban.do?announcementID=${row.id}')" />
			</jstl:if>
		</display:column>
	</security:authorize>

</display:table>

<jstl:forEach var="a" items="${announcements}">
	<jstl:if test="${a.banned == true}">
		<jstl:set var="aux" value="${1}" />
	</jstl:if>
</jstl:forEach>

<jstl:if test="${aux == 1}">
	<p style="color: #31F600; font-size: 20px;">
		<b><spring:message code="announcement.banned.green" /></b>
	</p>
</jstl:if>