<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="${requestURI}" modelAttribute="announcement">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="authorCustomer" />
	<form:hidden path="acceptedCustomers" />
	<form:hidden path="applications" />
	<form:hidden path="banned" />
	<form:hidden path="receivedComments" />
	
	<!--  -->

	<acme:textbox code="announcement.title" path="title" />

	<acme:textbox code="announcement.description" path="description" />

	<acme:textbox code="announcement.originPlace.address" path="originPlace.address" />
	
	<acme:textbox code="announcement.originPlace.gpsCoordinates" path="originPlace.gpsCoordinates" 
											placeholder="+-ddd.d.., +-ddd.d.."/>
	
	<acme:textbox code="announcement.destinationPlace.address" path="destinationPlace.address" />
	
	<acme:textbox code="announcement.destinationPlace.gpsCoordinates" path="destinationPlace.gpsCoordinates"
											placeholder="+-ddd.d.., +-ddd.d.." />
	
	<acme:textbox code="announcement.plannedMoment" path="plannedMoment" placeholder="dd/MM/yyyy HH:mm"/>
	
	<!--  -->

	<acme:submit name="save" code="announcement.save" />

	<acme:cancel url="" code="announcement.cancel" />

</form:form>