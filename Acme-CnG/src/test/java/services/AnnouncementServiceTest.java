
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Announcement;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class AnnouncementServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private AnnouncementService	announcementService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is authenticated as an administrator must be able to:
	 * Ban an offer or a request that he or she finds inappropriate.
	 * Such offers and re-quests must not be displayed to a general audience,
	 * only to the administrators and the customer who posted it.
	 * 
	 * En este caso de uso un administador puede banear un announcement que considere inapropiado.
	 * Si el announcement ya esta baneado, salta error, as� como cuando un customer o un usuario
	 * no auntentificado intenta acceder al m�todo.
	 */
	public void announcementBanTest(final String username, final int announcementId, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			final Announcement announcement = this.announcementService.findOne(announcementId);

			this.announcementService.ban(announcement);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an administrator must be able to:
	 * Ban an offer or a request that he or she finds inappropriate.
	 * Such offers and re-quests must not be displayed to a general audience,
	 * only to the administrators and the customer who posted it.
	 * 
	 * En este caso de uso se lleva a cabo el "filtrado" de los announcements que
	 * se encuentra baneados. Para forzar el error se comprobar� el tama�o de la colecci�n
	 * de announcements filtrados con diferentes actores autentificados.
	 */
	public void filterBanAnnouncements(final String username, final int tam, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			Collection<Announcement> announcements = this.announcementService.findAll();

			announcements = this.announcementService.filterAnnouncementsWithBan(announcements);

			Assert.isTrue(announcements.size() == tam);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ---------------------------------------------------------------

	@Test
	public void driverAnnouncementBan() {
		final Object testingData[][] = {
			// Ban announcement con customer -> false
			{
				"customer1", 1224, IllegalArgumentException.class
			},
			// Ban announcement con usuario no autentificado -> false
			{
				null, 1224, IllegalArgumentException.class
			},
			// Ban announcement con admin -> true
			{
				"admin", 1224, null
			},

			// Ban announcement ya baneada -> false
			{
				"admin", 1228, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.announcementBanTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverFilterBanAnnouncements() {
		final Object testingData[][] = {
			// Filtro autentificado con customer2 (no tiene ning�n announcement baneado), 'tam' correcto -> true
			{
				"customer2", 7, null
			},
			// Filtro autentificado con customer4 (offer4 est� baneada), 'tam' correcto -> true
			{
				"customer4", 8, null
			},
			// Filtro autentificado con customer4 (offer4 est� baneada), 'tam' incorrecto -> false
			{
				"customer4", 7, IllegalArgumentException.class
			},
			// Filtro autentificado con admin (tiene que poder ver todas, baneadas o no baneadas), 'tam' correcto -> true
			{
				"admin", 11, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.filterBanAnnouncements((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
