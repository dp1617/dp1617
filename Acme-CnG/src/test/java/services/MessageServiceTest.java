
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Actor;
import domain.Message;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class MessageServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private MessageService	messageService;
	@Autowired
	private ActorService	actorService;


	// Templates --------------------------------------------------------------

	/*
	 * Exchange messages with other actors
	 * 
	 * En este caso de uso se creará un mensaje. Se forzará el error cuando se intente
	 * realizar con un usuario no autentificado, con el título o el cuerpo del mensaje vacíos,
	 * con unos attachments que no coincidan con el pattern o cuando el receptor del mensaje no exista.
	 */
	public void createMessageTest(final String username, final String recipient, final String title, final String text, final Collection<String> attachments, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			final Actor r = this.actorService.findActorByUsername(recipient);

			this.authenticate(username);

			final Message message = new Message();

			message.setSender(this.actorService.findByPrincipal());
			message.setRecipient(r);
			message.setTitle(title);
			message.setText(text);
			message.setAttachments(attachments);

			this.messageService.save(message);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * List the messages that he or she’s got and reply to them
	 * List the messages that he or she’s got and forward them
	 * Erase his or her messages, which requires previous confirmation
	 * 
	 * En este caso de uso se listaran los mensajes enviados y recibidos por el usuario.
	 * Para forzar el error se accederá con un usuario no autentificado.
	 */
	public void listMessagesTest(final String username, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			this.authenticate(username);

			Collection<Message> receivedMessages = new ArrayList<>();
			Collection<Message> sentMessages = new ArrayList<>();
			final Collection<Message> allMessages = new ArrayList<>();

			Actor actor;
			actor = this.actorService.findByPrincipal();

			receivedMessages = actor.getReceivedMessages();
			sentMessages = actor.getSentMessages();

			allMessages.addAll(receivedMessages);
			allMessages.addAll(sentMessages);

			Assert.notEmpty(allMessages);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Forward a message that he or she's got
	 * 
	 * En este caso de uso se responderá a uno de los mensajes recibidos por el usuario.
	 * Para forzar el error se accederá con un usuario no autentificado,
	 * se responde a un mensaje que no pertenece al usuario, se responde a un mensaje
	 * que no existe.
	 */
	public void replyMessageTest(final String username, final int messageId, final String title, final String text, final Collection<String> attachments, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			Message m;

			this.authenticate(username);

			m = this.messageService.reply(messageId);
			m.setTitle(title);
			m.setText(text);
			m.setAttachments(attachments);

			this.messageService.save(m);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Forward a message that he or she's got
	 * 
	 * En este caso de uso se reenviará uno de los mensajes recibidos por el usuario.
	 * Para forzar el error se accederá con un usuario no autentificado,
	 * se reenviará un mensaje que no pertenece al usuario, se responde a un mensaje
	 * que no existe o se eligirá un destinatario no válido
	 */
	public void forwardMessageTest(final String username, final String recipient, final int messageId, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			Message m;
			final Actor r = this.actorService.findActorByUsername(recipient);

			this.authenticate(username);

			m = this.messageService.forward(messageId);
			m.setRecipient(r);
			this.messageService.save(m);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	/*
	 * Erase his or her messages, which requires previous confirmation
	 * 
	 * En este caso de uso se borrará un mensaje del propio usuario.
	 * Se forzará el error cuando el usuario no esté autentificado,
	 * si el mensaje no le pertenece o si la ID del mensaje no es válida
	 */
	public void deleteMessageTest(final String username, final int messageId, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			this.authenticate(username);

			final Message m = this.messageService.findOne(messageId);
			final Actor actor = this.actorService.findByPrincipal();

			if (actor.equals(m.getSender()))
				this.messageService.deleteSent(m);

			if (actor.equals(m.getRecipient()))
				this.messageService.deleteReceived(m);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ---------------------------------------------------------------

	@SuppressWarnings("unchecked")
	@Test
	public void driverCreateMessage() {
		final Collection<String> attachementsValid = new ArrayList<>();
		attachementsValid.add("https://www.attachmenttest.com");
		final Collection<String> attachementsNotValid = new ArrayList<>();
		attachementsNotValid.add("notvalidattachment");
		final Object testingData[][] = {
			// Create mensaje con customer y atributos validos -> true
			{
				"customer1", "customer2", "Titulo test", "Text test", attachementsValid, null
			},
			// Create mensaje con campo text a null -> false
			{
				"customer1", "customer2", "Titulo test", null, attachementsValid, IllegalArgumentException.class
			},
			// Create mensaje con campo title a null -> false
			{
				"customer1", "customer2", null, "Text test", attachementsValid, IllegalArgumentException.class
			},
			// Create mensaje con attachments no válidos -> false
			{
				"customer1", "customer2", "Titulo test", "Text test", attachementsNotValid, IllegalArgumentException.class
			},
			// Create mensaje hacia un usuario que no existe -> false
			{
				"customer1", "customer1234324", "Titulo test", "Text test", attachementsValid, IllegalArgumentException.class
			},
			// Create mensaje con usuario no autentificado -> false
			{
				null, "customer2", "Titulo test", "Text test", attachementsValid, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.createMessageTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Collection<String>) testingData[i][4], (Class<?>) testingData[i][5]);

	}
	@Test
	public void driverListMessages() {
		final Object testingData[][] = {
			// List de mensajes de un usuario -> true
			{
				"customer1", null
			},
			// List de mensajes de un usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.listMessagesTest((String) testingData[i][0], (Class<?>) testingData[i][1]);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void driverReplyMessage() {
		final Collection<String> attachementsValid = new ArrayList<>();
		attachementsValid.add("https://www.attachmenttest.com");
		final Object testingData[][] = {
			// Usuario y mensaje válidos -> true
			{
				"customer1", 1204, "Title test", "Text test", attachementsValid, null
			},
			//Usuario final no autentificado y final mensaje válido -> false
			{
				null, 1204, "Title test", "Text test", attachementsValid, IllegalArgumentException.class
			},
			//Reply a un mensaje que no pertenece al usuario -> false
			{
				"customer1", 1203, "Title test", "Text test", attachementsValid, IllegalArgumentException.class
			},
			// Reply a un mensaje que no existe -> false
			{
				"customer1", 1122, "Title test", "Text test", attachementsValid, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.replyMessageTest((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Collection<String>) testingData[i][4], (Class<?>) testingData[i][5]);

	}
	@Test
	public void driverForwardMessage() {
		final Object testingData[][] = {
			//Usuario y mensaje válidos -> true
			{
				"customer1", "customer3", 1204, null
			},
			//Usuario no autentificado y mensaje válido -> false
			{
				null, "customer2", 1204, IllegalArgumentException.class
			},
			// Forward un mensaje que no pertenece al usuario -> false
			{
				"customer1", "customer2", 1203, IllegalArgumentException.class
			},
			// Forward un mensaje que no existe -> false
			{
				"customer1", "customer2", 1122, IllegalArgumentException.class
			},
			// Forward a un destinatario no válido -> false
			{
				"customer1", "customer21112", 1204, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.forwardMessageTest((String) testingData[i][0], (String) testingData[i][1], (int) testingData[i][2], (Class<?>) testingData[i][3]);

	}

	@Test
	public void driverDeleteMessage() {
		final Object testingData[][] = {
			// Borrar mensaje que le pertenece -> true
			{
				"customer1", 1204, null
			},
			// Borrar mensaje como usuario no autentificado -> true
			{
				null, 1204, IllegalArgumentException.class
			},
			// Borrar mensaje que no le pertenece -> false
			{
				"customer2", 1204, IllegalArgumentException.class
			},
			// Banear mensaje con ID no válida -> false
			{
				"customer1", 1122, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.deleteMessageTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);

	}
}
