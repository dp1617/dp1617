
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Banner;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class BannerServiceTest extends AbstractTest {

	@Autowired
	private BannerService	bannerService;


	// Templates --------------------------------------------------------------
	/*
	 * Watch a welcome page with a banner that publicises Acme Car’n go!
	 * 
	 * En este caso se mostrará el banner que hay creado en el sistema. Es accesible para cualquier usuario, tanto autenticado
	 * como no. El error se obtendrá cuando la url no sea válida.
	 */

	public void watchBannerTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.bannerService.showBanner();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	public void editBannerTest(final String username, final String picture, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			final Banner banner = this.bannerService.getBanner();

			this.bannerService.checkURL(picture);

			banner.setPicture(picture);

			this.bannerService.save(banner);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverWatchBannerTest() {
		final Object testingData[][] = {
			//watch banner sin autenticarse -> true
			{
				null, null
			},
			// Watch banner con customer -> true
			{
				"customer1", null
			},
			// Watch banner con admin -> true
			{
				"admin", null
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.watchBannerTest((String) testingData[i][0], (Class<?>) testingData[i][1]);

	}
	@Test
	public void driverEditBannerTest() {
		final Object testingData[][] = {
			//edit banner con url correcta y siendo admin-> true
			{
				"admin", "http://www.bmw.hu/content/dam/bmw/common/all-models/m-series/m6-gran-coupe/2015/model-card/BMW-M6-Gran-Coupe_ModelCard.png", null
			},
			// Edit banner ain autenticarse -> false
			{
				null, "http://www.bmw.hu/content/dam/bmw/common/all-models/m-series/m6-gran-coupe/2015/model-card/BMW-M6-Gran-Coupe_ModelCard.png", IllegalArgumentException.class
			},
			// Edit a banner siendo customer-> false
			{
				"customer1", "http://www.bmw.hu/content/dam/bmw/common/all-models/m-series/m6-gran-coupe/2015/model-card/BMW-M6-Gran-Coupe_ModelCard.png", IllegalArgumentException.class
			},
			//Edit banner con picture incorrecta -> false
			{
				"admin", "definitelyNotAnUrl", IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.editBannerTest((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);

	}
}
