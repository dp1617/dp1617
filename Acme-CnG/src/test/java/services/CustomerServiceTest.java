
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Announcement;
import forms.CustomerForm;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private AnnouncementService		announcementService;
	@Autowired
	private CustomerService			customerService;
	@Autowired
	private AdministratorService	administratorService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is not authenticated must be able to:
	 * Watch a welcome page with a banner that publicises Acme Car n go
	 * and a button to register as a customer or to login
	 * 
	 * En este caso de uso una persona no autentificado entra en la web, teniendo las
	 * opciones de hacer login en la pagina o de registrarse como nuevo usuario.
	 * Para forzar error basta con no rellenar alguno de los campos obligatorios,
	 * si el pattern del telefono y del email no son los correctos, o si
	 * no se aceptan los t�rminos y condiciones de uso.
	 */
	public void customerRegisterTest(final String username, final String name, final String phone, final String email, final String username1, final String password, final String secondPassword, final Boolean checkBox, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			this.authenticate(username);

			Assert.isTrue(username == null);

			final CustomerForm customer = new CustomerForm();

			customer.setName(name);
			customer.setPhone(phone);
			customer.setEmail(email);
			customer.setUsername(username1);
			customer.setPassword(password);
			customer.setSecondPassword(secondPassword);
			customer.setCheckBox(checkBox);

			this.customerService.reconstruct(customer);

			this.customerService.checkIsCorrectEmail(customer);
			this.customerService.checkPhonePattern(customer);
			this.customerService.checkStringsAreNotNull(customer);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Vista para ver todos los customers
	 * 
	 * En este caso de uso se listan los customers desde su correspondiente vista
	 * si nos logeamos como administrador. Para provocar el error intentamos acceder
	 * al listado de customers siendo un customer o un usuario no autentificado.
	 */
	public void listCustomerTest(final String username, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.administratorService.checkIfAdministrator();

			this.customerService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Vista para ver los customers aceptados de un announcement
	 * 
	 * En este caso de uso se listan los customers aceptados en un announcement. Funciona correctamente
	 * si abrimos la vista desde un administrador o el customer que crea ese announcement.
	 * Para forzar el error se intenta acceder con un usuario no autentificado, con un customer que no sea
	 * creador de ese announcement o cuando el ID del announcement no es el correcto.
	 */
	public void listAcceptedCustomersOnAnnouncementsTest(final String username, final int announcementId, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			Announcement announcement;

			this.authenticate(username);

			this.customerService.checkIfCustomer();

			announcement = this.announcementService.findOne(announcementId);

			Assert.isTrue(announcement.getAuthorCustomer().getUserAccount().getUsername().equals(username));

			announcement.getAcceptedCustomers();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ---------------------------------------------------------------

	@Test
	public void driverCustomerRegisterTest() {
		final Object testingData[][] = {
			// Registrar correctamente un customer -> true
			{
				null, "Name Customer 23", "666554433", "customer23@email.com", "customer23", "customer23pass", "customer23pass", true, null
			},
			// Registrar un customer autentificado como customer -> false
			{
				"customer1", "Name Customer 24", "666554431", "customer24@email.com", "customer24", "customer24pass", "customer24pass", true, IllegalArgumentException.class
			},
			// Registrar un customer autentificado como admin -> false
			{
				"admin", "Name Customer 25", "666554435", "customer25@email.com", "customer25", "customer25pass", "customer25pass", true, IllegalArgumentException.class
			},
			// Registrar correctamente otro customer -> true
			{
				null, "Name Customer 40", "+32613129182", "customer40@email.com", "customer40", "customer40pass", "customer40pass", true, null
			},
			// Registro con pattern de telefono incorrecto -> false
			{
				null, "Name Customer 23", "65", "customer23@email.com", "customer23", "customer23pass", "customer23pass", true, IllegalArgumentException.class
			},
			// Registro con pattern de email incorrecto -> false
			{
				null, "Name Customer 23", "666554433", "emailmal", "customer23", "customer23pass", "customer23pass", true, IllegalArgumentException.class
			},
			// Falta algun atributo obligatorio -> false
			{
				null, "", "", "", "customer23", "customer23pass", "customer23pass", true, IllegalArgumentException.class
			},
			// No se aceptan los terminos y condiciones de uso -> false
			{
				null, "Name Customer 23", "666554433", "emailmal", "customer23", "customer23pass", "customer23pass", false, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.customerRegisterTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(Boolean) testingData[i][7], (Class<?>) testingData[i][8]);
	}

	@Test
	public void driverListCustomerTest() {
		final Object testingData[][] = {
			// Se accede con admin -> true
			{
				"admin", null
			},
			// Se accede con customer -> false
			{
				"customer1", IllegalArgumentException.class
			},
			//  Se accede con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listCustomerTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverListAcceptedCustomersOnAnnouncementTest() {
		final Object testingData[][] = {
			// Se accede con el customer autor del announcement -> true
			{
				"customer1", 1218, null
			},
			//  Se accede con user no autentificado -> false
			{
				null, 1218, IllegalArgumentException.class
			},
			//  Se accede con un customer que no es autor del announcement -> false
			{
				"customer4", 1218, IllegalArgumentException.class
			},
			//  announcementId no valido -> false
			{
				"customer1", 21132, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listAcceptedCustomersOnAnnouncementsTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
