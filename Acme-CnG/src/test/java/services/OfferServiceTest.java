
package services;

import java.util.Calendar;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Customer;
import domain.DestinationPlace;
import domain.Offer;
import domain.OriginPlace;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class OfferServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private OfferService	offerService;
	@Autowired
	private CustomerService	customerService;


	// Templates --------------------------------------------------------------

	/*
	 * Post a offer in which he or she informs that he or she wishes to move
	 * from a place to another one and would like to find someone with whom he
	 * or she can share the trip.
	 * 
	 * En este caso de uso se listar�n todas las offers del sistema.
	 * Es accesible para cualquier usuario autentificado. Para forzar el error se
	 * intenta acceder con un usuario no autentificado.
	 */
	public void offerListTest(final String username, final Class<?> expected) { //TODO: �COMPROBAR BAN?

		Class<?> caught = null;

		try {

			this.authenticate(username);
			this.offerService.findAll();
			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Post a offer in which he or she informs that he or she wishes to move
	 * from a place to another one and would like to find someone with whom he
	 * or she can share the trip.
	 * 
	 * En este caso de uso se listan las offers que pertenecen al 'customer' que
	 * se encuentra actualmente autentificado, por lo tanto es accesible para cualquier 'customer'.
	 * Para forzar el error se intenta acceder con un administrador y con un usuario no autentificado.
	 */
	public void offerMyListTest(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			Customer customer = new Customer();

			this.authenticate(username);

			this.offerService.checkIfCustomer();

			customer = this.customerService.getCustomerByUsername(username);

			this.offerService.getOffersByCustomer(customer);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	/*
	 * Post a offer in which he or she informs that he or she wishes to move
	 * from a place to another one and would like to find someone with whom he
	 * or she can share the trip.
	 * 
	 * En este caso de uso se postea (crea) una 'offer' de manera que pase a estar en la base de dats
	 * y sea accesible para todos los 'customers' del sistema. S�lo es accesible para los usuarios autentificados
	 * como 'customer', por lo que para forzar el error se ha intentado llevar a cabo la acci�n con un usuario
	 * no autentificado, con un usuario autentificado como 'administrator' o bien con par�metros inv�lidos.
	 */
	public void offerCreateTest(final String username, final String title, final String description, final Date moment, OriginPlace originPlace, DestinationPlace destinationPlace, final String originAddress, final String gpsOrigin,
		final String destinationAddress, final String gpsDestination, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			originPlace = new OriginPlace();
			destinationPlace = new DestinationPlace();

			originPlace.setAddress(originAddress);
			originPlace.setGpsCoordinates(gpsOrigin);
			destinationPlace.setAddress(destinationAddress);
			destinationPlace.setGpsCoordinates(gpsDestination);

			final Offer offer = new Offer();

			offer.setTitle(title);
			offer.setDescription(description);
			offer.setPlannedMoment(moment);
			offer.setOriginPlace(originPlace);
			offer.setDestinationPlace(destinationPlace);

			this.offerService.checkIfCustomer();
			this.offerService.checkDate(offer);
			this.offerService.checkPatternGps(originPlace);
			this.offerService.checkPatternGps(destinationPlace);
			this.offerService.save(offer);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Search for offers and offers using a single keyword that must appear
	 * somewhere in their titles, descriptions, or places.
	 * 
	 * En este caso de uso se lleva a cabo la b�squeda de offers por medio de una palabra clave.
	 * Es accesible para cualquier usuario autentificado, por lo que para forzar el error se intenta
	 * acceder con un usuario no autentificado.
	 */
	public void offerSearch(final String username, final String keyword, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			this.offerService.checkIfActor();

			this.offerService.searchOffersByKeyword(keyword);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ---------------------------------------------------------------

	@Test
	public void driverOfferList() {
		final Object testingData[][] = {
			// List de offer con customer -> true
			{
				"customer1", null
			},
			// List de offer con admin -> true
			{
				"admin", null
			},
			// List de offer con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.offerListTest((String) testingData[i][0], (Class<?>) testingData[i][1]);

	}

	@Test
	public void driverMyListTest() {
		final Object testingData[][] = {
			// Mi lista con customer -> true
			{
				"customer1", null
			},
			// Mi lista con admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			// Mi lista con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.offerMyListTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverOfferCreate() {

		Date goodOne = new Date();
		final Calendar c = Calendar.getInstance();
		c.setTime(goodOne);
		c.add(Calendar.DATE, 10);
		goodOne = c.getTime();

		Date badOne = new Date();
		final Calendar c1 = Calendar.getInstance();
		c1.setTime(badOne);
		c1.add(Calendar.DATE, -10);
		badOne = c1.getTime();

		final OriginPlace origin = new OriginPlace();
		final DestinationPlace destination = new DestinationPlace();

		final Object testingData[][] = {
			// Creaci�n con customer y propiedades correctas -> true
			{
				"customer1", "title", "description", goodOne, origin, destination, "origin address", "1.30007, -113.61082", "destination address", "17.85281, -10.87204", null
			},
			// Creaci�n con admin -> false
			{
				"admin", "title", "description", goodOne, origin, destination, "origin address", "1.30007, -113.61082", "destination address", "17.85281, -10.87204", IllegalArgumentException.class
			},
			// Creaci�n con usuario no autentificado -> false
			{
				null, "title", "description", goodOne, origin, destination, "origin address", "1.30007, -113.61082", "destination address", "17.85281, -10.87204", IllegalArgumentException.class
			},
			// Creaci�n con formato de coordenadas incorrecto -> false
			{
				"customer2", "title", "description", goodOne, origin, destination, "origin address", "200,4", "destination address", "-121", IllegalArgumentException.class
			},
			// Creaci�n con fecha superior a la actual -> false
			{
				"customer3", "title", "description", badOne, origin, destination, "origin address", "1.30007, -113.61082", "destination address", "17.85281, -10.87204", IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.offerCreateTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Date) testingData[i][3], (OriginPlace) testingData[i][4], (DestinationPlace) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (String) testingData[i][8], (String) testingData[i][9], (Class<?>) testingData[i][10]);
	}

	@Test
	public void driverOfferSearch() {
		final Object testingData[][] = {
			// Search offer con customer -> true
			{
				"customer1", "address", null
			},
			// Search offer con admin -> true
			{
				"admin", "address", null
			},
			// Search offer con usuario no autentificado -> false
			{
				null, "address", IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.offerSearch((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
