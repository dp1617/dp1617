
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Actor;
import domain.Announcement;
import domain.Comment;
import domain.Commentable;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private CommentService		commentService;
	@Autowired
	private CustomerService		customerService;
	@Autowired
	private ActorService		actorService;
	@Autowired
	private AnnouncementService	announcementService;


	// Templates --------------------------------------------------------------

	/*
	 * Post a comment on another actor, on an offer, or a request.
	 * 
	 * En este caso de uso se crear� un comentario. Se forzar� el error cuando se intente
	 * realizar con un usuario no autentificado, falta alg�n atributo a completar,
	 * se crea un comentario hacia uno mismo o el n� de estrellas no es correcto.
	 */
	public void createCommentTest(final String username, final String commentable, final String title, final String text, final int stars, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			final Commentable c = this.customerService.getCustomerByUsername(commentable);

			this.authenticate(username);

			final Comment comment = new Comment();

			comment.setActor(this.customerService.findByPrincipal());
			comment.setCommentable(c);
			comment.setTitle(title);
			comment.setText(text);
			comment.setStars(stars);

			this.commentService.save(comment);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * P ost a comment on another actor, on an offer, or a request.
	 * 
	 * En este caso de uso se listaran los comentarios creados hacia actores y anouncements.
	 * Para forzar el error se acceder� con un usuario no autentificado y se comprobar�
	 * que el filtro de comentarios baneados funciona correctamente.
	 */
	public void listCommentSentTest(final String username, final int tam, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			this.authenticate(username);

			Collection<Comment> commentsAnnouncements = new ArrayList<>();
			Collection<Comment> comments = new ArrayList<>();

			comments = this.commentService.getAllWrittenCommentsToActorByAnActor();
			commentsAnnouncements = this.commentService.getAllWrittenCommentsToAnnouncementsByAnActor();

			Assert.isTrue(tam == commentsAnnouncements.size() + comments.size());

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Post a comment on another actor, on an offer, or a request.
	 * 
	 * En este caso de uso se listaran los comentarios que ha recibido un actor por
	 * parte de otros actores. Para forzar el error se intenar� acceder con un usuario
	 * no autentificado y se comprobar� que el filtro de comentarios baneados funciona
	 * correctamente.
	 */
	public void listCommentReceivedTest(final String username, final int tam, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			Actor actor;

			this.authenticate(username);

			Collection<Comment> comments = new ArrayList<>();
			Collection<Comment> createdComments = new ArrayList<>();

			actor = this.actorService.findByPrincipal();
			comments = actor.getReceivedComments();
			createdComments = this.commentService.filterCommentsWithBan(comments);

			Assert.isTrue(tam == createdComments.size());

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Post a comment on another actor, on an offer, or a request.
	 * 
	 * En este caso de uso se listaran los comentarios que ha recibido un announcement.
	 * Para forzar el error se intentar� acceder con un usuario no autentificado,
	 * utilizando una ID no v�lida, y se comprobar� el buen funcionamiento del filtro
	 * de comentarios baneados.
	 */
	public void listCommentAnnouncementReceivedTest(final String username, final int announcementId, final int tam, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			Announcement announcement;
			this.authenticate(username);
			Collection<Comment> comments = new ArrayList<>();
			Collection<Comment> createdComments = new ArrayList<>();

			announcement = this.announcementService.findOne(announcementId);
			comments = announcement.getReceivedComments();
			createdComments = this.commentService.filterCommentsWithBan(comments);

			Assert.isTrue(tam == createdComments.size());

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Post a comment on another actor, on an offer, or a request.
	 * 
	 * En este caso de uso se listaran los comentarios creados por otros actores hacia otros actores.
	 * Para forzar el error se intentar� acceder con un usuario distinto a administrador
	 * y se comprobar� el buen funcionamiento del filtro de comentarios baneados.
	 */
	public void listCommentSendToActorsByActorsTest(final String username, final int tam, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			this.authenticate(username);

			Collection<Comment> comments = new ArrayList<>();

			comments = this.commentService.getAllWrittenCommentsToActors();

			Assert.isTrue(tam == comments.size());

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Ban a comment that he or she finds inappropriate. Such comments must
	 * not be displayed to a general audience, only to the administrators and the
	 * actor who post-ed it.
	 * 
	 * En este caso de uso baneara un comentario inapropiado creado por un actor.
	 * Se forzar� el error cuando el usuario sea customer o no est� autentificado, o bien
	 * si el comentario ya ha sido previamente baneado.
	 */
	public void banCommentTest(final String username, final int commentId, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			this.authenticate(username);

			final Comment c = this.commentService.findOne(commentId);

			this.commentService.ban(c);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ---------------------------------------------------------------

	@Test
	public void driverCreateComment() {
		final Object testingData[][] = {
			// Create comment con customer y atributos validos -> true
			{
				"customer1", "customer2", "Titulo test", "Text test", 4, null
			},
			// Create comment con campo text a null -> false
			{
				"customer1", "customer2", "Titulo test", null, 4, IllegalArgumentException.class
			},
			// Create comment con puntuacion fuera de rango -> false
			{
				"customer1", "customer2", "Titulo test", "Text test", 7, IllegalArgumentException.class
			},
			// Create comment hacia el propio usuario logueado -> false
			{
				"customer1", "customer1", "Titulo test", "Text test", 5, IllegalArgumentException.class
			},
			// Create comment con usuario no autentificado -> false
			{
				null, "customer1", "Titulo test", "Text test", 7, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.createCommentTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (int) testingData[i][4], (Class<?>) testingData[i][5]);

	}

	@Test
	public void driverListCommentSent() {
		final Object testingData[][] = {
			// List de comentarios enviados con customer -> true
			{
				"customer1", 3, null
			},
			// List de comentarios enviados con usuario no autentificado -> true
			{
				null, 3, IllegalArgumentException.class
			},
			// List de comentarios con filtro erroneo -> false
			{
				"customer1", 2, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.listCommentSentTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	@Test
	public void driverListCommentReceived() {
		final Object testingData[][] = {
			// List de comentarios recibidos con customer -> true
			{
				"customer2", 2, null
			},
			// List de comentarios recibidos con usuario no autentificado -> true
			{
				null, 3, IllegalArgumentException.class
			},
			// List de comentarios recibidos con filtro erroneo -> false
			{
				"customer2", 5, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.listCommentReceivedTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	@Test
	public void driverListCommentAnnouncementReceived() {
		final Object testingData[][] = {
			// Lista de comentarios que ha recibido un announcement con customer -> true
			{
				"customer1", 1225, 1, null
			},
			// Lista de comentarios que ha recibido un announcement con usuario no autentificado -> true
			{
				null, 1225, 3, IllegalArgumentException.class
			},
			// Lista de comentarios que ha recibido un announcement con filtro erroneo -> false
			{
				"customer1", 1225, 5, IllegalArgumentException.class
			},
			// Lista de comentarios que ha recibido un announcement con un id que no existe -> false
			{
				"customer1", 9999, 5, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.listCommentAnnouncementReceivedTest((String) testingData[i][0], (int) testingData[i][1], (int) testingData[i][2], (Class<?>) testingData[i][3]);

	}

	@Test
	public void driverListCommentSendToActorsByActors() {
		final Object testingData[][] = {
			// Lista de comentarios con admin -> true
			{
				"admin", 5, null
			},
			// Lista de comentarios con usuario no autentificado -> true
			{
				null, 5, IllegalArgumentException.class
			},
			// Lista de comentarios con filtro erroneo -> false
			{
				"admin", 3, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.listCommentSendToActorsByActorsTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	@Test
	public void driverbanComment() {
		final Object testingData[][] = {
			// Banear comentario como admin -> true
			{
				"admin", 1213, null
			},
			// Banear comentario como usuario no autentificado -> true
			{
				null, 1213, IllegalArgumentException.class
			},
			// Banear comentario como customer -> false
			{
				"customer", 1213, IllegalArgumentException.class
			},
			// Banear comentario ya baneado -> false
			{
				"admin", 1214, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.banCommentTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);

	}
}
