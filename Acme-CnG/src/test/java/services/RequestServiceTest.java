
package services;

import java.util.Calendar;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Customer;
import domain.DestinationPlace;
import domain.OriginPlace;
import domain.Request;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class RequestServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private RequestService	requestService;
	@Autowired
	private CustomerService	customerService;


	// Templates --------------------------------------------------------------

	/*
	 * Post a request in which he or she informs that he or she wishes to move
	 * from a place to another one and would like to find someone with whom he
	 * or she can share the trip.
	 * 
	 * En este caso de uso se listar�n todas las requests del sistema.
	 * Es accesible para cualquier usuario autentificado. Para forzar el error se
	 * intenta acceder con un usuario no autentificado.
	 */
	public void requestListTest(final String username, final Class<?> expected) { //TODO: �COMPROBAR BAN?

		Class<?> caught = null;

		try {

			this.authenticate(username);
			this.requestService.findAll();
			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Post a request in which he or she informs that he or she wishes to move
	 * from a place to another one and would like to find someone with whom he
	 * or she can share the trip.
	 * 
	 * En este caso de uso se listan las requests que pertenecen al 'customer' que
	 * se encuentra actualmente autentificado, por lo tanto es accesible para cualquier 'customer'.
	 * Para forzar el error se intenta acceder con un administrador y con un usuario no autentificado.
	 */
	public void requestMyListTest(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			Customer customer = new Customer();

			this.authenticate(username);

			this.requestService.checkIfCustomer();

			customer = this.customerService.getCustomerByUsername(username);

			this.requestService.getRequestsByCustomer(customer);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	/*
	 * Post a request in which he or she informs that he or she wishes to move
	 * from a place to another one and would like to find someone with whom he
	 * or she can share the trip.
	 * 
	 * En este caso de uso se postea (crea) una 'request' de manera que pase a estar en la base de dats
	 * y sea accesible para todos los 'customers' del sistema. S�lo es accesible para los usuarios autentificados
	 * como 'customer', por lo que para forzar el error se ha intentado llevar a cabo la acci�n con un usuario
	 * no autentificado, con un usuario autentificado como 'administrator' o bien con par�metros inv�lidos.
	 */
	public void requestCreateTest(final String username, final String title, final String description, final Date moment, OriginPlace originPlace, DestinationPlace destinationPlace, final String originAddress, final String gpsOrigin,
		final String destinationAddress, final String gpsDestination, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			originPlace = new OriginPlace();
			destinationPlace = new DestinationPlace();

			originPlace.setAddress(originAddress);
			originPlace.setGpsCoordinates(gpsOrigin);
			destinationPlace.setAddress(destinationAddress);
			destinationPlace.setGpsCoordinates(gpsDestination);

			final Request request = new Request();

			request.setTitle(title);
			request.setDescription(description);
			request.setPlannedMoment(moment);
			request.setOriginPlace(originPlace);
			request.setDestinationPlace(destinationPlace);

			this.requestService.checkIfCustomer();
			this.requestService.checkDate(request);
			this.requestService.checkPatternGps(originPlace);
			this.requestService.checkPatternGps(destinationPlace);
			this.requestService.save(request);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Search for offers and requests using a single keyword that must appear
	 * somewhere in their titles, descriptions, or places.
	 * 
	 * En este caso de uso se lleva a cabo la b�squeda de requests por medio de una palabra clave.
	 * Es accesible para cualquier usuario autentificado, por lo que para forzar el error se intenta
	 * acceder con un usuario no autentificado.
	 */
	public void requestSearch(final String username, final String keyword, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			this.requestService.checkIfActor();

			this.requestService.searchRequestsByKeyword(keyword);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ---------------------------------------------------------------

	@Test
	public void driverRequestList() {
		final Object testingData[][] = {
			// List de request con customer -> true
			{
				"customer1", null
			},
			// List de request con admin -> true
			{
				"admin", null
			},
			// List de request con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.requestListTest((String) testingData[i][0], (Class<?>) testingData[i][1]);

	}

	@Test
	public void driverMyListTest() {
		final Object testingData[][] = {
			// Mi lista con customer -> true
			{
				"customer1", null
			},
			// Mi lista con admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			// Mi lista con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.requestMyListTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverRequestCreate() {

		Date goodOne = new Date();
		final Calendar c = Calendar.getInstance();
		c.setTime(goodOne);
		c.add(Calendar.DATE, 10);
		goodOne = c.getTime();

		Date badOne = new Date();
		final Calendar c1 = Calendar.getInstance();
		c1.setTime(badOne);
		c1.add(Calendar.DATE, -10);
		badOne = c1.getTime();

		final OriginPlace origin = new OriginPlace();
		final DestinationPlace destination = new DestinationPlace();

		final Object testingData[][] = {
			// Creaci�n con customer y propiedades correctas -> true
			{
				"customer1", "title", "description", goodOne, origin, destination, "origin address", "1.30007, -113.61082", "destination address", "17.85281, -10.87204", null
			},
			// Creaci�n con admin -> false
			{
				"admin", "title", "description", goodOne, origin, destination, "origin address", "1.30007, -113.61082", "destination address", "17.85281, -10.87204", IllegalArgumentException.class
			},
			// Creaci�n con usuario no autentificado -> false
			{
				null, "title", "description", goodOne, origin, destination, "origin address", "1.30007, -113.61082", "destination address", "17.85281, -10.87204", IllegalArgumentException.class
			},
			// Creaci�n con formato de coordenadas incorrecto -> false
			{
				"customer2", "title", "description", goodOne, origin, destination, "origin address", "200,4", "destination address", "-121", IllegalArgumentException.class
			},
			// Creaci�n con fecha superior a la actual -> false
			{
				"customer3", "title", "description", badOne, origin, destination, "origin address", "1.30007, -113.61082", "destination address", "17.85281, -10.87204", IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.requestCreateTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Date) testingData[i][3], (OriginPlace) testingData[i][4], (DestinationPlace) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (String) testingData[i][8], (String) testingData[i][9], (Class<?>) testingData[i][10]);
	}

	@Test
	public void driverRequestSearch() {
		final Object testingData[][] = {
			// Search request con customer -> true
			{
				"customer1", "address", null
			},
			// Search request con admin -> true
			{
				"admin", "address", null
			},
			// Search request con usuario no autentificado -> false
			{
				null, "address", IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.requestSearch((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
