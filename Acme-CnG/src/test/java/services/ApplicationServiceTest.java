
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Announcement;
import domain.Application;
import domain.Customer;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ApplicationServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private ApplicationService	applicationService;
	@Autowired
	private CustomerService		customerService;
	@Autowired
	private AnnouncementService	announcementService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is authenticated as a customer must be able to:
	 * 
	 * Apply for an offer or a request, which must be accepted by the customer
	 * who post-ed it. Applications can be pending, accepted, or denied.
	 * 
	 * En este caso de uso un customer env�a una solicitud a un announcement (offer o request).
	 * Para forzar el error se intenta acceder con diversos actores, utilizando
	 * una ID de announcement inv�lida, aplicar por segunda vez, o aplicar a un announcement
	 * expirado.
	 */
	public void sendApplicationTest(final String username, final int announcementId, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			Announcement announcement;

			this.authenticate(username);

			this.customerService.checkIfCustomer();

			announcement = this.announcementService.findOne(announcementId);

			this.applicationService.sendApplication(announcement);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a customer must be able to:
	 * 
	 * Apply for an offer or a request, which must be accepted by the customer
	 * who post-ed it. Applications can be pending, accepted, or denied.
	 * 
	 * En este caso de uso se listan las applications recibidadas de los announcements
	 * del customer actualmente autentificado. Para forzar el error se intenta acceder
	 * con un usuario no autentificado y un administrador.
	 */
	public void listApplicationReceivedTest(final String username, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			Customer customer = new Customer();

			this.authenticate(username);
			this.customerService.checkIfCustomer();
			customer = this.customerService.getCustomerByUsername(username);

			this.applicationService.getApplicationsSentToAnnouncements(customer);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a customer must be able to:
	 * 
	 * Apply for an offer or a request, which must be accepted by the customer
	 * who post-ed it. Applications can be pending, accepted, or denied.
	 * 
	 * En este caso de uso se listan las applications enviadas a diferentes announcements.
	 * Para forzar el error se intenta acceder con un usuario no autentificado y un administrador.
	 */
	public void listApplicationSendTest(final String username, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			Customer customer = new Customer();

			this.authenticate(username);

			this.customerService.checkIfCustomer();

			customer = this.customerService.getCustomerByUsername(username);

			customer.getApplications();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a customer must be able to:
	 * 
	 * Apply for an offer or a request, which must be accepted by the customer
	 * who post-ed it. Applications can be pending, accepted, or denied.
	 * 
	 * En este caso de uso se aceptara una petici�n de un customer hacia un announcement.
	 * Para forzar el error se intentar� aceptar una petici�n de un announcement del cual
	 * el customer actual no es autor, con un usuario no autentificado y con un administrador.
	 */
	public void acceptApplicationTest(final String username, final int applicationId, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			Application application;

			this.authenticate(username);

			this.customerService.checkIfCustomer();

			application = this.applicationService.findOne(applicationId);

			Assert.isTrue(applicationService.checkIfIsHerApplication(application));
			this.applicationService.acceptRequest(application);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a customer must be able to:
	 * 
	 * Apply for an offer or a request, which must be accepted by the customer
	 * who post-ed it. Applications can be pending, accepted, or denied.
	 * 
	 * En este caso de uso se denegar� una petici�n de un customer hacia un announcement.
	 * Para forzar el error se intentar� denegar una petici�n de un announcement del cual
	 * el customer actual no es autor, con un usuario no autentificado y con un administrador.
	 */
	public void denyApplicationTest(final String username, final int applicationId, final Class<?> expected) { //TODO:

		Class<?> caught = null;

		try {

			Application application;

			this.authenticate(username);

			this.customerService.checkIfCustomer();

			application = this.applicationService.findOne(applicationId);

			Assert.isTrue(applicationService.checkIfIsHerApplication(application));
			this.applicationService.cancelRequest(application);
			;

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ---------------------------------------------------------------

	@Test
	public void driverSendApplicationTest() {
		final Object testingData[][] = {
			// Enviar aplicacion con customer, planned momment correcto, no se ha realizado antes una aplicacion para ese announcement y el announcement no es del customer -> true
			{
				"customer1", 1226, null
			},
			// Enviar aplicacion con usuario no autentificado -> false
			{
				null, 1111, IllegalArgumentException.class
			},
			// Enviar aplicacion con admin -> false
			{
				"admin", 1111, IllegalArgumentException.class
			},
			// Enviar aplicacion con un id de announcement que no existe -> false
			{
				"customer1", 7425, IllegalArgumentException.class
			},
			// Enviar aplicacion en el que el announcement pertenece al customer logueado -> false
			{
				"customer1", 1224, IllegalArgumentException.class
			},
			// Enviar aplicacion cuando ya hay una aplicacion pendiente realizado por el mismo customer a la misma announcement -> false
			{
				"customer1", 1219, IllegalArgumentException.class
			},
			// Enviar aplicacion con el planned momment expirado -> false
			{
				"customer1", 1223, IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.sendApplicationTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverListApplicationsReceivedTest() {
		final Object testingData[][] = {
			// Mis aplicaciones recibidas con customer -> true
			{
				"customer1", null
			},
			// Mis aplicaciones recibidas con admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			//  Mis aplicaciones recibidas con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listApplicationReceivedTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverListApplicationsSendTest() {
		final Object testingData[][] = {
			// Mis aplicaciones enviadas con customer -> true
			{
				"customer2", null
			},
			//  Mis aplicaciones enviadas con admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			//  Mis aplicaciones enviadas con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listApplicationSendTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverAcceptApplicationTest() {
		final Object testingData[][] = {
			//  Aceptar solicitud con customer al que va dirigida la aplicacion -> true
			{
				"customer2", 1239, null
			},
			//  Aceptar solicitud con customer al que no va dirigida la aplicacion -> false
			{
				"customer2", 1243, IllegalArgumentException.class
			},
			//  Aceptar solicitud con administrator -> false
			{
				"admin", 1239, IllegalArgumentException.class
			},
			//  Aceptar solicitud con usuario no autentificado -> false
			{
				null, 1111, IllegalArgumentException.class
			},
			//  Aceptar solicitud ya aceptada -> false
			{
				"customer1", 1238, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.acceptApplicationTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverDenyApplicationTest() {
		final Object testingData[][] = {
			//  Denegar solicitud con customer al que va dirigida la aplicacion -> true
			{
				"customer2", 1239, null
			},
			//  Denegar solicitud con customer al que no va dirigida la aplicacion -> false
			{
				"customer2", 1243, IllegalArgumentException.class
			},
			//  Denegar solicitud con usuario no autentificado -> false
			{
				null, 1111, IllegalArgumentException.class
			},
			//  Denegar solicitud con administrator -> false
			{
				"admin", 1239, IllegalArgumentException.class
			},
			//  Denegar solicitud ya aceptada -> false
			{
				"customer1", 1238, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.denyApplicationTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
