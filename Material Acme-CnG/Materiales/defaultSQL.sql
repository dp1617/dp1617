drop database if exists `Acme-CnG`;
create database `Acme-CnG`;
grant select, insert, update, delete on `Acme-CnG`.* to 'acme-user'@'%';
grant select, insert, update, delete, create, drop, references, index, alter, create temporary tables, 
lock tables, create view, create routine, alter routine, execute, trigger, show view
    on `Acme-CnG`.* to 'acme-manager'@'%';